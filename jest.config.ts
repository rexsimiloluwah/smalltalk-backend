import type { Config } from "@jest/types";

// Sync object
const config: Config.InitialOptions = {
  preset: "ts-jest",
  testEnvironment: "node",
  moduleFileExtensions: ["ts", "js"],
  moduleDirectories: ["src", "node_modules"],
  rootDir: "src",
  testRegex: ".*\\.spec\\.ts$",
  transform: {
    "^.+\\.(t|j)s$": "ts-jest",
  },
  coverageDirectory: "../coverage",
  collectCoverageFrom: ["**/*.(t|j)s"],
  coveragePathIgnorePatterns: [
    "/node_modules/",
    "common",
    "<rootDir>/main.ts",
    "index.ts",
    "jestGlobalMocks.ts",
    ".interface.ts",
    ".module.ts",
    ".mock.ts",
    ".model.ts",
    ".entity.ts",
    ".dto.ts",
    ".input.ts",
    ".spec.ts",
    ".test.ts",
    ".decorator.ts",
    ".types.ts",
    ".output.ts",
    ".strategy.ts",
    ".args.ts",
  ],
};
export default config;
