migrate:
	npx prisma migrate dev --name $(name)

prisma-studio:
	npx prisma studio 

describe-rds-instances:
	aws rds describe-db-instances --query "DBInstances[*].[Endpoint.Address,Endpoint.Port,DBName,MasterUsername,MasterUserPassword]" --output table