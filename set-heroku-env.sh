#!/bin/bash
# Usage: ./set-heroku-env.sh <filepath> i.e. ./set-heroku-env.sh .env 


if test -f "$1"; then 
    input=$1 
else 
    if test -f ".env"; then 
        input=".env"
    fi 
fi 

echo -e "Setting heroku env variables from: $input"

envars=""
# read the content of the file 
while read -r line; do 
    envvars+=" $line"
done < "$input"
eval heroku config:set "$envvars"
