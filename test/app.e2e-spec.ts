import { Test, TestingModule } from '@nestjs/testing'
import { INestApplication, ValidationPipe } from '@nestjs/common'
import * as request from 'supertest'
import { AppModule } from './../src/app.module'
import { PrismaService } from './../src/prisma/prisma.service'
import { CreateUserInputDTO } from './../src/users/dto'

describe('AppController (e2e)', () => {
 let app: INestApplication
 let prisma: PrismaService

 const testUsersInput: CreateUserInputDTO[] = [
  {
   email: 'bolatinubu@gmail.com',
   password: 'secret',
   firstname: 'Bola',
   lastname: 'Tinubu',
   username: 'bolatinubu',
  },
  {
   email: 'peterobi@gmail.com',
   password: 'secret',
   firstname: 'Peter',
   lastname: 'Obi',
   username: 'peterobi',
  },
 ]

 beforeAll(async () => {
  const moduleFixture: TestingModule = await Test.createTestingModule({
   imports: [AppModule],
  }).compile()

  app = moduleFixture.createNestApplication()
  // add the global validation pipes
  app.useGlobalPipes(
   new ValidationPipe({
    whitelist: true,
   }),
  )

  // get the prisma service
  prisma = app.get(PrismaService)
  await prisma.cleanDb()
  await app.init()
 })

 afterAll(async () => {
  await app.close()
  await prisma.cleanDb()
  await prisma.$disconnect()
 })

 describe('Testing auth (e2e)', () => {
  it('Should register a new user', () => {
   return request(app.getHttpServer())
    .post('/graphql')
    .send({
     query: `mutation{
            register(data:{
                email:"${testUsersInput[0].email}",
                password:"${testUsersInput[0].password}",
                firstname:"${testUsersInput[0].firstname}",
                lastname:"${testUsersInput[0].lastname}",
                username:"${testUsersInput[0].username}"
            }){
                email,
                password,
                firstname,
                lastname,
                username
            }
            }`,
    })
    .expect(200)
    .expect((res) => {
     expect(res.body.data.register).toBeDefined()
     expect(res.body.data.register).toEqual({
      email: testUsersInput[0].email,
      password: null,
      firstname: testUsersInput[0].firstname,
      lastname: testUsersInput[0].lastname,
      username: testUsersInput[0].username,
     })
    })
  })

  it('Should fail to register an existing user', () => {
   return request(app.getHttpServer())
    .post('/graphql')
    .send({
     query: `mutation{
            register(data:{
                email:"${testUsersInput[0].email}",
                password:"${testUsersInput[0].password}",
                firstname:"${testUsersInput[0].firstname}",
                lastname:"${testUsersInput[0].lastname}",
                username:"${testUsersInput[0].username}"
            }){
                email,
                password,
                firstname,
                lastname,
                username
            }
            }`,
    })
    .expect((res) => {
     expect(res.body.errors).toBeDefined()
     expect(res.body.errors[0].message).toEqual('User already exists.')
    })
  })

  // Testing login e2e (email/password and google login)
  it('Should login an existing user', () => {
   return request(app.getHttpServer())
    .post('/graphql')
    .send({
     query: `mutation{
              login(data:{
                  email:"${testUsersInput[0].email}",
                  password:"${testUsersInput[0].password}"
              }){
                  user{
                    id,
                      email,
                      username,
                      firstname,
                      lastname
                  },
                  token{
                      accessToken,
                      refreshToken
                  }
              }
              }`,
    })
    .expect(200)
    .expect((res) => {
     expect(res.body.data.login.user).toBeDefined()
     expect(res.body.data.login.token).toBeDefined()
     const { user } = res.body.data.login
     expect(user.id).toBeDefined()
     expect(user.email).toEqual(testUsersInput[0].email)
    })
  })
 })

 // Testing the user module
 describe('Testing users (e2e)', () => {
  let accessToken: string

  beforeAll(async () => {
   // Register some test users
   for (const user of testUsersInput) {
    await request(app.getHttpServer())
     .post('/graphql')
     .send({
      query: `mutation{
            register(data:{
                email:"${user.email}",
                password:"${user.password}",
                firstname:"${user.firstname}",
                lastname:"${user.lastname}",
                username:"${user.username}"
            }){
                email,
                password,
                firstname, 
                lastname, 
                username
            }
            }`,
     })
   }

   // Login a test user and obtain the access token
   const response = await request(app.getHttpServer())
    .post('/graphql')
    .send({
     query: `mutation{
            login(data:{
                email:"${testUsersInput[0].email}",
                password:"${testUsersInput[0].password}"
            }){
                user{
                  id,
                },
                token{
                    accessToken, 
                    refreshToken
                }
            }
            }`,
    })
   accessToken = response.body.data.login.token.accessToken
  })

  it('Should fetch authenticated user', () => {
   return request(app.getHttpServer())
    .post('/graphql')
    .send({
     query: `query{
          me{
            id,
            email, 
            username, 
            firstname, 
            lastname
          }
        }`,
    })
    .set('Authorization', `Bearer ${accessToken}`)
    .expect(200)
    .expect((res) => {
     expect(res.body.data.me).toBeDefined()
     const { id, email, username, lastname, firstname } = res.body.data.me
     expect(id).toBeDefined()
     expect(email).toEqual(testUsersInput[0].email)
     expect(username).toEqual(testUsersInput[0].username)
     expect(lastname).toEqual(testUsersInput[0].lastname)
     expect(firstname).toEqual(testUsersInput[0].firstname)
    })
  })

  it('Should find user with email=peterobi@gmail.com', () => {
   return request(app.getHttpServer())
    .post('/graphql')
    .send({
     query: `query{
          user(where:{email:"peterobi@gmail.com"}){
            id, 
            email, 
            username, 
            firstname, 
            lastname
          }
        }`,
    })
    .expect(200)
    .expect((res) => {
     // console.log(res.body);
     expect(res.body.data.user).toBeDefined()
     const { id, email, username, firstname, lastname } = res.body.data.user
     expect(id).toBeDefined()
     expect(email).toEqual(testUsersInput[1].email)
     expect(username).toEqual(testUsersInput[1].username)
     expect(lastname).toEqual(testUsersInput[1].lastname)
     expect(firstname).toEqual(testUsersInput[1].firstname)
    })
  })

  it('Should return all users', () => {
   return request(app.getHttpServer())
    .post('/graphql')
    .send({
     query: `query{
          users(where:{}){
            id,
            email, 
            firstname, 
            lastname,
            username
          }
        }`,
    })
    .expect(200)
    .expect((res) => {
     expect(res.body.data.users).toBeDefined()
     expect(res.body.data.users.length).toEqual(testUsersInput.length)
    })
  })

  it('Should return users with username=peterobi', () => {
   return request(app.getHttpServer())
    .post('/graphql')
    .send({
     query: `query{
          users(where:{username:"peterobi"}){
            id,
            email, 
            firstname, 
            lastname,
            username
          }
        }`,
    })
    .expect((res) => {
     // console.log(res.body.data.users);
     expect(res.body.data.users).toBeDefined()
     expect(res.body.data.users.length).toEqual(1)
     const { id, email, username, firstname, lastname } = res.body.data.users[0]
     expect(id).toBeDefined()
     expect(email).toEqual(testUsersInput[1].email)
     expect(username).toEqual(testUsersInput[1].username)
     expect(lastname).toEqual(testUsersInput[1].lastname)
     expect(firstname).toEqual(testUsersInput[1].firstname)
    })
  })

  it('Should update a user profile', () => {
   return request(app.getHttpServer())
    .post('/graphql')
    .send({
     query: `mutation{
            updateUser(data:{
                username: "updated-test-user"
            }){
                id, 
                email, 
                username
            }
            }`,
    })
    .set('Authorization', `Bearer ${accessToken}`)
    .expect(200)
    .expect((res) => {
     expect(res.body.data.updateUser).toBeDefined()
     const updatedUser = res.body.data.updateUser
     expect(updatedUser.username).toEqual('updated-test-user')
    })
  })

  it('Should delete user', () => {
   return request(app.getHttpServer())
    .post('/graphql')
    .send({
     query: `mutation{
          deleteUser{
            id, 
            email,
            username,
            firstname, 
            lastname
          }
        }`,
    })
    .set('Authorization', `Bearer ${accessToken}`)
    .expect(200)
    .expect((res) => {
     expect(res.body.data.deleteUser).toBeDefined()
    })
    .then(() => {
     request(app.getHttpServer())
      .post('/graphql')
      .send({
       query: `query{
                user(where:{id:1}){
                  id, 
                  email,
                  username,
                  firstname, 
                  lastname
                }
          }`,
      })
      .expect((res) => {
       expect(res.body.data.user).toEqual(null)
      })
    })
  })
 })

 describe('Testing posts (e2e)', () => {
  let accessToken: string
  beforeAll(async () => {
   // Register some test users
   for (const user of testUsersInput) {
    await request(app.getHttpServer())
     .post('/graphql')
     .send({
      query: `mutation{
            register(data:{
                email:"${user.email}",
                password:"${user.password}",
                firstname:"${user.firstname}",
                lastname:"${user.lastname}",
                username:"${user.username}"
            }){
                email,
                password,
                firstname, 
                lastname, 
                username
            }
            }`,
     })
   }

   // Login a test user and obtain the access token
   const response = await request(app.getHttpServer())
    .post('/graphql')
    .send({
     query: `mutation{
            login(data:{
                email:"${testUsersInput[0].email}",
                password:"${testUsersInput[0].password}"
            }){
                user{
                  id,
                },
                token{
                    accessToken, 
                    refreshToken
                }
            }
            }`,
    })
   accessToken = response.body.data.login.token.accessToken
  })

  // TODO: testing file upload e2e
  it('Should fetch all posts', () => {
   return request(app.getHttpServer())
    .post('/graphql')
    .send({
     query: `query{
          posts(args:{take:5,skip:0},orderBy:{createdAt:asc}){
            pageInfo{
              hasNextPage, 
              totalCount, 
              totalPages, 
              endCursorId
            },
            posts{
              id, 
              title,
              createdAt,
              updatedAt
            }
          }
        }`,
    })
    .expect(200)
    .expect((res) => {
     expect(res.body.data.posts).toBeDefined()
     expect(res.body.data.posts.pageInfo).toBeDefined()
     const { pageInfo, posts } = res.body.data.posts
     expect(pageInfo).toMatchObject({
      hasNextPage: false,
      endCursorId: null,
      totalCount: 0,
      totalPages: 0,
     })
     expect(posts).toEqual([])
    })
  })

  it('Should find posts', () => {
   return request(app.getHttpServer())
    .post('/graphql')
    .send({
     query: `query{
            findPosts(where:{}){
              id, 
              title,
              createdAt, 
              updatedAt
            }
        }`,
    })
    .expect(200)
    .expect((res) => {
     //console.log(res.body.data);
     expect(res.body.data.findPosts).toEqual([])
    })
  })

  it('Should fetch posts by tag', () => {
   return request(app.getHttpServer())
    .post('/graphql')
    .send({
     query: `query{
          postsByTag(tag:"finance"){
            id, 
              title,
              createdAt, 
              updatedAt
          }
        }`,
    })
    .expect((res) => {
     expect(res.body.data.postsByTag).toEqual([])
    })
  })

  it('Should fetch posts by category', () => {
   return request(app.getHttpServer())
    .post('/graphql')
    .send({
     query: `query{
          postsByCategory(category:"finance"){
              id, 
              title,
              createdAt, 
              updatedAt
          }
        }`,
    })
    .expect((res) => {
     expect(res.body.data.postsByCategory).toEqual([])
    })
  })
 })

 //  describe('Testing comments (e2e)', () => {
 //   return
 //  })
})
