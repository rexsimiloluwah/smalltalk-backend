#!/bin/bash 

STACK_NAME=smalltalks3bucket 
REGION=us-west-2 
BUCKET_NAME=smalltalkdev

aws cloudformation deploy \
    --region $REGION \
    --stack-name $STACK_NAME \
    --template-file cloudformation/create-s3-bucket.yml \
    --no-fail-on-empty-changeset \
    --capabilities CAPABILITY_NAMED_IAM \
    --parameter-overrides BucketName=$BUCKET_NAME 

# show the bucket ARN
if [ $? -eq 0 ]; then 
    aws cloudformation list-exports \
        --query "Exports[?starts_with(Name, 'SmalltalkS3BucketArn')].Value" \
        --region $REGION 
fi 