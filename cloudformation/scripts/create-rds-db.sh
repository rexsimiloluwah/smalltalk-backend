#!/bin/bash 

STACK_NAME=smalltalkdb
REGION=us-west-2 
DB_INSTANCE_CLASS=db.t2.micro
DB_INSTANCE_ID=smalltalkdb

DB_USERNAME=$(cat ~/.github/rds-db-username)
DB_PASSWORD=$(cat ~/.github/rds-db-password)

aws cloudformation deploy \
    --region $REGION \
    --stack-name ${STACK_NAME}-stack \
    --template-file cloudformation/create-rds-db.yml \
    --no-fail-on-empty-changeset \
    --capabilities CAPABILITY_NAMED_IAM \
    --parameter-overrides DBInstanceID=$DB_INSTANCE_ID \
      DBInstanceClass=$DB_INSTANCE_CLASS \
      DBUsername=$DB_USERNAME \
      DBPassword=$DB_PASSWORD 

# If the deploy was successful, show the ARN of the created DB instance 
if [ $? -eq 0 ]; then 
    aws cloudformation list-exports \
        --query "Exports[?starts_with(Name, 'SmalltalkDBInstanceArn')].Value" \
        --region $REGION 
fi