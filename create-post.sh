#!/bin/sh
curl -X POST 'http://localhost:3333/graphql' \
--form 'operations="{
	\"query\": \"mutation ($data: CreatePostInputDTO!, $file:Upload!) { createPost(data: $data, file:$file) }\",
	\"variables\": {
		\"data\": {
			\"title\": \"Test title\",
			\"description\": \"Test description\"
		},
		\"file\": null
	}
}"' \
--form 'map="{\"0\": [\"variables.file\"]}"' \
--form '0=@"./file.txt"'