import {
 Injectable,
 ForbiddenException,
 HttpException,
 BadRequestException,
 HttpStatus,
} from '@nestjs/common'
import { User } from './users.entity'
import { PrismaService } from '../prisma/prisma.service'
import { ConfigService } from '@nestjs/config'
import { CreateUserInputDTO, UpdateUserInputDTO, UserWhereUniqueInputDTO } from './dto'
import {
 PrismaClientKnownRequestError,
 PrismaClientUnknownRequestError,
} from '@prisma/client/runtime'
import * as argon from 'argon2'
import { Follow } from './follow.entity'
import { POSTS_INCLUDE } from '../posts/posts.service'
import { UpdateUserProfileInputDTO } from './dto/update-user-profile.input'
import { UserProfile } from './profile.entity'
import { UpdatePasswordInputDTO } from './dto/update-password.input'

export const USER_SELECT = {
 id: true,
 email: true,
 firstname: true,
 lastname: true,
 createdAt: true,
 updatedAt: true,
 username: true,
 emailVerified: true,
 profileImageUrl: true,
}

export const USER_PROFILE_SELECT = {
 id: true,
 bio: true,
 twitterUrl: true,
 facebookUrl: true,
 instagramUrl: true,
 phoneNumber: true,
}

@Injectable()
export class UsersService {
 constructor(private prisma: PrismaService, private config: ConfigService) {}
 async findUsers(where: UserWhereUniqueInputDTO): Promise<User[]> {
  return this.prisma.user.findMany({
   where: where,
  })
 }

 // Create user
 async createUser(data: CreateUserInputDTO): Promise<User> {
  const { password } = data
  // generate the password hash
  const hashedPassword = await argon.hash(password)
  // save the new user to the database
  try {
   const newUser = await this.prisma.user.create({
    data: {
     ...data,
     password: hashedPassword,
    },
    select: USER_SELECT,
   })
   //console.log(newUser);

   // create the user profile
   await this.prisma.profile.create({
    data: {
     user: {
      connect: {
       id: newUser.id,
      },
     },
    },
   })

   // return the user
   return newUser
  } catch (error) {
   console.error(error)
   if (error instanceof PrismaClientKnownRequestError) {
    if (error.code === 'P2002') {
     throw new ForbiddenException('User already exists.')
    }
   }
   throw new HttpException(error.message, HttpStatus.INTERNAL_SERVER_ERROR)
  }
 }

 // Update user
 async updateUser(where: UserWhereUniqueInputDTO, data: UpdateUserInputDTO): Promise<User> {
  try {
   const updatedUser = await this.prisma.user.update({
    where: where,
    data: data,
   })
   delete updatedUser.password
   return updatedUser
  } catch (error) {
   if (error.status) {
    throw error
   }

   if (error instanceof PrismaClientUnknownRequestError) {
    throw new BadRequestException('User does not exist')
   }

   throw new HttpException(error.message, HttpStatus.INTERNAL_SERVER_ERROR)
  }
 }

 // Update user password
 async updateUserPassword(
  where: UserWhereUniqueInputDTO,
  data: UpdatePasswordInputDTO,
 ): Promise<User> {
  try {
   const { password } = data
   // generate the password hash
   const hashedPassword = await argon.hash(password)

   const updatedUser = await this.prisma.user.update({
    where: where,
    data: {
     password: hashedPassword,
    },
   })
   delete updatedUser.password
   return updatedUser
  } catch (error) {
   if (error.status) {
    throw error
   }

   if (error instanceof PrismaClientUnknownRequestError) {
    throw new BadRequestException('User does not exist')
   }

   throw new HttpException(error.message, HttpStatus.INTERNAL_SERVER_ERROR)
  }
 }

 // Update user profile
 async updateUserProfile(
  where: UserWhereUniqueInputDTO,
  data: UpdateUserProfileInputDTO,
 ): Promise<UserProfile> {
  try {
   const updatedUserProfile = await this.prisma.profile.update({
    where: where,
    data: data,
   })

   return updatedUserProfile
  } catch (error) {
   if (error.status) {
    throw error
   }

   if (error instanceof PrismaClientUnknownRequestError) {
    throw new BadRequestException('User profile does not exist')
   }

   throw new HttpException(error.message, HttpStatus.INTERNAL_SERVER_ERROR)
  }
 }

 // Find user
 async findUser(where: UserWhereUniqueInputDTO): Promise<User> {
  const user = await this.prisma.user.findFirst({
   where: where,
   include: {
    profile: {
     select: USER_PROFILE_SELECT,
    },
    posts: {
     include: POSTS_INCLUDE,
     orderBy: {
      createdAt: 'desc',
     },
    },
    followers: {
     include: {
      user: {
       select: USER_SELECT,
      },
      follower: {
       select: USER_SELECT,
      },
     },
    },
    following: {
     include: {
      user: {
       select: USER_SELECT,
      },
      follower: {
       select: USER_SELECT,
      },
     },
    },
   },
  })

  return user
 }

 // Follow user
 async followUser(userId: number, followId: number): Promise<Follow> {
  const follow = await this.prisma.follow.create({
   data: {
    user: {
     connect: {
      id: followId,
     },
    },
    follower: {
     connect: {
      id: userId,
     },
    },
   },
   include: {
    user: {
     select: USER_SELECT,
    },
   },
  })

  return follow
 }

 // Unfollow user
 async unfollowUser(userId: number, followId: number): Promise<Follow> {
  const follow = await this.prisma.follow.delete({
   where: {
    userId_followerId: {
     userId: followId,
     followerId: userId,
    },
   },
   include: {
    user: {
     select: USER_SELECT,
    },
   },
  })

  return follow
 }

 // Delete user
 async deleteUser(where: UserWhereUniqueInputDTO): Promise<User> {
  try {
   const deletedUser = await this.prisma.user.delete({
    where: where,
   })
   delete deletedUser.password
   return deletedUser
  } catch (error) {
   if (error.status) {
    throw error
   }

   if (error instanceof PrismaClientUnknownRequestError) {
    throw new BadRequestException('User does not exist')
   }

   throw new HttpException(error.message, HttpStatus.INTERNAL_SERVER_ERROR)
  }
 }
}
