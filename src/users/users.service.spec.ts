import { Test, TestingModule } from '@nestjs/testing'
import { UsersService, USER_PROFILE_SELECT } from './users.service'
import { PrismaService } from '../prisma/prisma.service'
import { ConfigService } from '@nestjs/config'
import { User } from './users.entity'
import { Post } from '../posts/posts.entity'
import { Follow } from './follow.entity'
import { UserProfile } from './profile.entity'
import { CreateUserInputDTO, UpdateUserInputDTO } from './dto'
import { USER_SELECT } from './users.service'
import { POSTS_INCLUDE } from '../posts/posts.service'

const testUserOne: User = {
 id: 1,
 email: 'test-user-1@gmail.com',
 username: 'username1',
 firstname: 'firstname1',
 lastname: 'lastname1',
 posts: [] as Post[],
 followers: [] as Follow[],
 following: [] as Follow[],
}

const testUserTwo: User = {
 id: 2,
 email: 'test-user-2@gmail.com',
 username: 'username2',
 firstname: 'firstname2',
 lastname: 'lastname2',
 posts: [] as Post[],
 followers: [] as Follow[],
 following: [] as Follow[],
}

const testUsers = [testUserOne, testUserTwo]

const createUserInput: CreateUserInputDTO = {
 email: 'test-user-2@gmail.com',
 username: 'username2',
 firstname: 'firstname2',
 lastname: 'lastname2',
 password: 'secret',
}

const updateUserInput: UpdateUserInputDTO = {
 username: 'updated-user',
}

describe('UsersService', () => {
 let usersService: UsersService
 let prismaService

 const prismaServiceMock = () => ({
  user: {
   findUnique: jest.fn(),
   findFirst: jest.fn(),
   findMany: jest.fn(),
   count: jest.fn(),
   create: jest.fn(),
   upsert: jest.fn(),
   update: jest.fn(),
   updateMany: jest.fn(),
   delete: jest.fn(),
   deleteMany: jest.fn(),
  },
  profile: {
   findUnique: jest.fn(),
   findFirst: jest.fn(),
   findMany: jest.fn(),
   count: jest.fn(),
   create: jest.fn(),
   upsert: jest.fn(),
   update: jest.fn(),
   updateMany: jest.fn(),
   delete: jest.fn(),
   deleteMany: jest.fn(),
  },
  $queryRaw: jest.fn(),
 })

 beforeEach(async () => {
  const module: TestingModule = await Test.createTestingModule({
   providers: [
    UsersService,
    {
     provide: PrismaService,
     useFactory: prismaServiceMock,
    },
    {
     provide: ConfigService,
     useValue: {
      get: jest.fn((key: string) => {
       if (key === 'JWT_SECRET_KEY') {
        return 'secret'
       }
       return null
      }),
     },
    },
   ],
  }).compile()

  usersService = module.get<UsersService>(UsersService)
  prismaService = module.get<PrismaService>(PrismaService)
 })

 it('should be defined', () => {
  expect(usersService).toBeDefined()
 })

 // Test create user service
 describe('createOneUser', () => {
  it('Should return the created user.', async () => {
   prismaService.user.create.mockReturnValue(testUserOne)
   prismaService.profile.create.mockReturnValue(new UserProfile())
   const result = await usersService.createUser(createUserInput)
   expect(result).toMatchObject(testUserOne)
  })
 })

 // Test find users service
 describe('findUsers', () => {
  it('Should return an array of users.', async () => {
   prismaService.user.findMany.mockReturnValue(testUsers)
   const result = await usersService.findUsers({})
   expect(prismaService.user.findMany).toHaveBeenCalledWith({ where: {} })
   expect(result).toMatchObject(testUsers)
  })
 })

 // Test find user service
 describe('findUser', () => {
  it('Should return user with id = 1', async () => {
   const whereQuery = { id: 1 }
   const expectedResult = testUsers.filter((user) => user.id === 1)
   prismaService.user.findFirst.mockReturnValue(expectedResult)
   const result = await usersService.findUser(whereQuery)
   expect(prismaService.user.findFirst).toHaveBeenCalledWith({
    where: whereQuery,
    include: {
     profile: {
      select: USER_PROFILE_SELECT,
     },
     posts: {
      include: POSTS_INCLUDE,
      orderBy: {
       createdAt: 'desc',
      },
     },
     followers: {
      include: {
       user: {
        select: USER_SELECT,
       },
       follower: {
        select: USER_SELECT,
       },
      },
     },
     following: {
      include: {
       user: {
        select: USER_SELECT,
       },
       follower: {
        select: USER_SELECT,
       },
      },
     },
    },
   })
   expect(result).toEqual(expectedResult)
  })
 })

 // Test update user
 describe('updateUser', () => {
  it("Should update the user's username", async () => {
   const updatedUser = {
    ...testUserOne,
    username: updateUserInput.username,
   }
   prismaService.user.update.mockReturnValue(updatedUser)
   const result = await usersService.updateUser({ id: 1 }, { username: updateUserInput.username })
   expect(result).toEqual(updatedUser)
  })
 })

 // Test delete user
 describe('deleteUser', () => {
  it('Should delete the user', async () => {
   prismaService.user.delete.mockReturnValue(testUserOne)
   const result = await usersService.deleteUser({ id: 1 })
   expect(result).toEqual(testUserOne)
  })
 })

 // Test follow user

 // Test unfollow user
})
