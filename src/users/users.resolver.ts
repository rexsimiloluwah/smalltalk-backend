import { UseGuards } from '@nestjs/common'
import { Context, Query, Resolver, Args, Mutation } from '@nestjs/graphql'
import { JwtAuthGuard } from '../auth/guards/jwt-auth.guard'
import { UpdateUserInputDTO, UserWhereUniqueInputDTO } from './dto'
import { UpdatePasswordInputDTO } from './dto/update-password.input'
import { UpdateUserProfileInputDTO } from './dto/update-user-profile.input'
import { Follow } from './follow.entity'
import { UserProfile } from './profile.entity'
import { User } from './users.entity'
import { UsersService } from './users.service'

@Resolver(() => User)
export class UsersResolver {
 constructor(private usersService: UsersService) {}

 @Query(() => User)
 @UseGuards(JwtAuthGuard)
 public async me(@Context() ctx): Promise<User> {
  const req = ctx.req
  const { user } = req
  console.log(user)
  return user
 }

 // Update user
 @Mutation(() => User)
 @UseGuards(JwtAuthGuard)
 public async updateUser(@Context() ctx, @Args('data') data: UpdateUserInputDTO): Promise<User> {
  const { user } = ctx.req
  return this.usersService.updateUser({ id: user.id }, data)
 }

 // Update user password
 @Mutation(() => User)
 @UseGuards(JwtAuthGuard)
 public async updateUserPassword(
  @Context() ctx,
  @Args('data') data: UpdatePasswordInputDTO,
 ): Promise<User> {
  const { user } = ctx.req
  return this.usersService.updateUserPassword({ id: user.id }, data)
 }

 // Update user profile
 @Mutation(() => UserProfile)
 @UseGuards(JwtAuthGuard)
 public async updateUserProfile(
  @Context() ctx,
  @Args('data') data: UpdateUserProfileInputDTO,
 ): Promise<UserProfile> {
  const { user } = ctx.req
  return this.usersService.updateUserProfile({ id: user.id }, data)
 }

 // Delete user
 @Mutation(() => User)
 @UseGuards(JwtAuthGuard)
 public async deleteUser(@Context() ctx): Promise<User> {
  const { user } = ctx.req
  return this.usersService.deleteUser({ id: user.id })
 }

 // Find user
 @Query(() => User, { name: 'user' })
 public async findUser(@Args('where') where: UserWhereUniqueInputDTO): Promise<User> {
  return this.usersService.findUser(where)
 }

 // List users
 @Query(() => [User], { name: 'users' })
 public async findUsers(@Args('where') where: UserWhereUniqueInputDTO): Promise<User[]> {
  return this.usersService.findUsers(where)
 }

 // Follow user
 @Mutation(() => Follow, { name: 'follow' })
 @UseGuards(JwtAuthGuard)
 public async followUser(
  @Args('where') where: UserWhereUniqueInputDTO,
  @Context() ctx,
 ): Promise<Follow> {
  const { user } = ctx.req
  return this.usersService.followUser(user.id, where.id)
 }

 // Unfollow user
 @Mutation(() => Follow, { name: 'unfollow' })
 @UseGuards(JwtAuthGuard)
 public async unfollowUser(
  @Args('where') where: UserWhereUniqueInputDTO,
  @Context() ctx,
 ): Promise<Follow> {
  const { user } = ctx.req
  return this.usersService.unfollowUser(user.id, where.id)
 }
}
