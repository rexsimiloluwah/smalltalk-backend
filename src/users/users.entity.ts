import { ObjectType, Field, ID } from '@nestjs/graphql'
import { Post } from '../posts/posts.entity'
import { Follow } from './follow.entity'
import { UserProfile } from './profile.entity'

@ObjectType()
export class User {
 @Field(() => ID, { nullable: false })
 id!: number

 @Field(() => String, { nullable: false })
 username!: string

 @Field(() => String, { nullable: false })
 firstname!: string

 @Field(() => String, { nullable: false })
 lastname!: string

 @Field(() => String, { nullable: false })
 email!: string

 @Field(() => String, { nullable: true })
 password?: string

 @Field(() => String, { nullable: true })
 profileImageUrl?: string

 @Field(() => Boolean, { nullable: true })
 emailVerified?: boolean

 @Field(() => [Post], { nullable: true })
 posts?: Post[]

 @Field(() => UserProfile, { nullable: true })
 profile?: UserProfile

 @Field(() => Date, { nullable: true })
 createdAt?: Date | string

 @Field(() => Date, { nullable: true })
 updatedAt?: Date | string

 @Field(() => [Follow], { nullable: true })
 followers?: Follow[]

 @Field(() => [Follow], { nullable: true })
 following?: Follow[]
}
