import { ObjectType, Field, ID } from '@nestjs/graphql'
import { User } from './users.entity'

@ObjectType()
export class UserProfile {
 @Field(() => ID, { nullable: false })
 id: number

 @Field(() => String, { nullable: true })
 bio?: string

 @Field(() => String, { nullable: true })
 twitterUrl?: string

 @Field(() => String, { nullable: true })
 facebookUrl?: string

 @Field(() => String, { nullable: true })
 instagramUrl?: string

 @Field(() => String, { nullable: true })
 phoneNumber?: string

 @Field(() => User, { nullable: true })
 user?: User

 @Field(() => ID, { nullable: true })
 userId?: number

 @Field(() => Date, { nullable: true })
 createdAt?: Date | string

 @Field(() => Date, { nullable: true })
 updatedAt?: Date | string
}
