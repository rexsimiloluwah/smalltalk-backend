import { ObjectType, Field, ID } from '@nestjs/graphql'
import { User } from './users.entity'

@ObjectType()
export class Follow {
 @Field(() => ID, { nullable: false })
 id: number

 @Field(() => ID, { nullable: true })
 userId?: number

 @Field(() => ID, { nullable: true })
 followerId?: number

 @Field(() => User, { nullable: true })
 user?: User

 @Field(() => User, { nullable: true })
 follower?: User

 @Field(() => Date, { nullable: true })
 createdAt: Date | string

 @Field(() => Date, { nullable: true })
 updatedAt: Date | string
}
