import { Test, TestingModule } from '@nestjs/testing'
import { UsersResolver } from './users.resolver'
import { UsersService } from './users.service'
import { User } from './users.entity'
import { Post } from '../posts/posts.entity'
import { Follow } from './follow.entity'

const testUserOne: User = {
 id: 1,
 email: 'test-user-1@gmail.com',
 username: 'username1',
 firstname: 'firstname1',
 lastname: 'lastname1',
 posts: [] as Post[],
 followers: [] as Follow[],
 following: [] as Follow[],
}

const testUserTwo: User = {
 id: 2,
 email: 'test-user-2@gmail.com',
 username: 'username2',
 firstname: 'firstname2',
 lastname: 'lastname2',
 posts: [] as Post[],
 followers: [] as Follow[],
 following: [] as Follow[],
}

const testUsers = [testUserOne, testUserTwo]

describe('UserssResolver', () => {
 let resolver: UsersResolver
 let usersService

 const usersServiceMock = () => ({
  createUser: jest.fn(),
  updateUser: jest.fn(),
  deleteUser: jest.fn(),
  findUser: jest.fn(),
  followUser: jest.fn(),
  unfollowUser: jest.fn(),
  findUsers: jest.fn(),
 })

 beforeEach(async () => {
  const module: TestingModule = await Test.createTestingModule({
   providers: [
    UsersResolver,
    {
     provide: UsersService,
     useFactory: usersServiceMock,
    },
   ],
  }).compile()

  resolver = module.get<UsersResolver>(UsersResolver)
  usersService = module.get<UsersService>(UsersService)
 })

 it('should be defined', () => {
  expect(resolver).toBeDefined()
 })

 // Test the find user resolver
 describe('findUserResolver', () => {
  it('Should return user', async () => {
   usersService.findUser.mockReturnValue(testUserOne)
   const result = await resolver.findUser({ id: 1 })
   expect(result).toEqual(testUserOne)
  })
 })

 // Test the find users resolver
 describe('findUsersResolver', () => {
  it('Should return users', async () => {
   usersService.findUsers.mockReturnValue(testUsers)
   const result = await resolver.findUsers({})
   expect(result).toEqual(testUsers)
  })
 })

 // Test the update users resolver
 // describe("updateUserResolver", () => {
 //   it("Should update user", async () => {
 //     const updatedUser = {
 //       ...testUserOne,
 //       username: "updated user",
 //     };
 //     usersService.updateUser.mockReturnValue();
 //     const result = await resolver.updateUser(
 //       { id: 1 },
 //       { username: "updated user" }
 //     );
 //     expect(usersService.updateUser).toHaveBeenCalledWith({
 //       where: { id: 1 },
 //       data: { username: "updated user" },
 //     });
 //     expect(result).toEqual(updatedUser);
 //   });
 // });

 // Test the delete user resolver
 // describe("deleteUserResolver", () => {
 //   it("Should delete user", async () => {
 //     usersService.deleteUser.mockReturnValue(testUserOne);
 //     const result = await resolver.deleteUser({ id: 1 });
 //     expect(result).toEqual(testUserOne);
 //   });
 // });
})
