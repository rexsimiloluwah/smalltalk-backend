import { Field, InputType } from '@nestjs/graphql'
import { IsNumber, IsOptional, IsString } from 'class-validator'

@InputType()
export class UserWhereUniqueInputDTO {
 @Field(() => Number, {
  nullable: true,
 })
 @IsOptional()
 @IsNumber()
 id?: number

 @Field(() => String, {
  nullable: true,
 })
 @IsString()
 @IsOptional()
 email?: string

 @Field(() => String, {
  nullable: true,
 })
 @IsString()
 @IsOptional()
 username?: string
}
