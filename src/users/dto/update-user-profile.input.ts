import { IsOptional, IsString } from 'class-validator'
import { InputType, Field } from '@nestjs/graphql'

@InputType()
export class UpdateUserProfileInputDTO {
 @Field(() => String, { nullable: true })
 @IsString()
 @IsOptional()
 bio?: string

 @Field(() => String, { nullable: true })
 @IsString()
 @IsOptional()
 twitterUrl?: string

 @Field(() => String, { nullable: true })
 @IsString()
 @IsOptional()
 instagramUrl?: string

 @Field(() => String, { nullable: true })
 @IsString()
 @IsOptional()
 facebookUrl?: string

 @Field(() => String, { nullable: true })
 @IsString()
 @IsOptional()
 phoneNumber?: string
}
