import { IsEmail, IsString, IsNotEmpty, IsOptional } from 'class-validator'
import { InputType, Field } from '@nestjs/graphql'

@InputType()
export class CreateUserInputDTO {
 @Field(() => String, { nullable: false })
 @IsEmail()
 @IsString()
 email: string

 @Field(() => String, { nullable: false })
 @IsString()
 @IsNotEmpty()
 username: string

 @Field(() => String, { nullable: false })
 @IsString()
 @IsNotEmpty()
 firstname: string

 @Field(() => String, { nullable: false })
 @IsString()
 @IsNotEmpty()
 lastname: string

 @Field(() => String, { nullable: false })
 @IsString()
 @IsNotEmpty()
 password: string

 @Field(() => String, { nullable: true })
 @IsOptional()
 profileImageUrl?: string

 @Field(() => Boolean, { nullable: true })
 @IsOptional()
 emailVerified?: boolean
}
