import { IsOptional, IsString } from 'class-validator'
import { InputType, Field } from '@nestjs/graphql'

@InputType()
export class UpdateUserInputDTO {
 @Field(() => String, { nullable: true })
 @IsString()
 @IsOptional()
 email?: string

 @Field(() => String, { nullable: true })
 @IsString()
 @IsOptional()
 username?: string

 @Field(() => String, { nullable: true })
 @IsString()
 @IsOptional()
 firstname?: string

 @Field(() => String, { nullable: true })
 @IsString()
 @IsOptional()
 lastname?: string

 @Field(() => String, { nullable: true })
 @IsString()
 @IsOptional()
 password?: string

 @Field(() => String, { nullable: true })
 @IsString()
 @IsOptional()
 hashedRefreshToken?: string
}
