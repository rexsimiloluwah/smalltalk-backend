import { IsOptional, IsString } from 'class-validator'
import { InputType, Field } from '@nestjs/graphql'

@InputType()
export class UpdatePasswordInputDTO {
 @Field(() => String, { nullable: true })
 @IsString()
 @IsOptional()
 password: string
}
