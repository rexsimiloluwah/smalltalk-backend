import { Injectable } from '@nestjs/common'
import { HttpException, HttpStatus } from '@nestjs/common'
import { paginate } from '../common/utils/paginate'
import { Post } from '../posts/posts.entity'
import { PrismaService } from '../prisma/prisma.service'
import { PaginatedFeedOutput, FeedPaginationArgs } from './dto'
import { POSTS_INCLUDE } from '../posts/posts.service'

@Injectable()
export class FeedService {
 constructor(private prisma: PrismaService) {}

 // Get user paginated feed
 async getUserFeed(
  paginationArgs: FeedPaginationArgs,
  topic: string,
  userId: number,
 ): Promise<PaginatedFeedOutput> {
  const { currentPage, pageSize } = paginationArgs
  try {
   let feedPosts: Post[] = []

   // Get posts of user's following
   // Firstly, fetch the user's following  data
   const userWithFollowing = await this.prisma.user.findFirst({
    where: {
     id: userId,
    },
    orderBy: {
     createdAt: 'desc',
    },
    include: {
     followers: true,
     following: true,
    },
   })

   let userFollowingPosts: Post[] = []

   if (userWithFollowing) {
    for (const follow of userWithFollowing.following) {
     if (follow.userId) {
      const posts = await this.prisma.post.findMany({
       where: {
        creatorId: follow.userId,
        category: {
         contains: topic.toLowerCase(),
        },
       },
       include: POSTS_INCLUDE,
      })

      // append to the userFollowingPosts array
      userFollowingPosts = [...userFollowingPosts, ...posts]
     }
    }
   }

   // Get user's posts
   const userPosts = await this.prisma.post.findMany({
    where: {
     creatorId: userId,
     category: {
      contains: topic.toLowerCase(),
     },
     createdAt: {
      gte: userFollowingPosts.length > 0 ? new Date(userFollowingPosts[0].createdAt) : new Date(),
     },
    },
    include: POSTS_INCLUDE,
   })

   // append to the feed posts
   feedPosts = [...feedPosts, ...userPosts]

   // append user following posts to the feed posts
   feedPosts = [...feedPosts, ...userFollowingPosts]

   // sort the current state of feed posts by createdAt
   feedPosts = feedPosts.sort((a, b) => {
    return new Date(b.createdAt).getTime() - new Date(a.createdAt).getTime()
   })

   // get the other posts
   const otherPosts = await this.prisma.post.findMany({
    where: {
     category: {
      contains: topic,
     },
     id: {
      notIn: feedPosts.map((post) => post.id),
     },
    },
    orderBy: {
     createdAt: 'desc',
    },
    include: POSTS_INCLUDE,
   })

   // append other posts to the feed posts
   feedPosts = [...feedPosts, ...otherPosts]

   // create the response
   const responseFeed = [...new Set(feedPosts)]

   // paginate response
   const paginatedResponse = paginate(responseFeed, currentPage, pageSize)

   return paginatedResponse
  } catch (error) {
   if (error.status) {
    throw error
   }

   throw new HttpException(error.message, HttpStatus.INTERNAL_SERVER_ERROR)
  }
 }

 // Get feed for a guest user
 async getGuestUserFeed(
  paginationArgs: FeedPaginationArgs,
  topic: string,
 ): Promise<PaginatedFeedOutput> {
  const { currentPage, pageSize } = paginationArgs
  try {
   const response = await this.prisma.post.findMany({
    where: {
     category: {
      contains: topic,
     },
    },
    orderBy: {
     likes: {
      _count: 'asc',
     },
    },
    include: POSTS_INCLUDE,
   })

   // paginate response
   const paginatedResponse = paginate(response, currentPage, pageSize)

   return paginatedResponse
  } catch (error) {
   if (error.status) {
    throw error
   }

   throw new HttpException(error.message, HttpStatus.INTERNAL_SERVER_ERROR)
  }
 }
}
