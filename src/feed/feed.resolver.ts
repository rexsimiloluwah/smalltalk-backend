import { Resolver, Query, Context, Args } from '@nestjs/graphql'
import { UseGuards } from '@nestjs/common'
import { Post } from '../posts/posts.entity'
import { JwtAuthGuard } from '../auth/guards/jwt-auth.guard'
import { FeedPaginationArgs, FeedWhereSelectInputDTO, PaginatedFeedOutput } from './dto'
import { FeedService } from './feed.service'

@Resolver()
export class FeedResolver {
 constructor(private feedService: FeedService) {}

 @Query(() => PaginatedFeedOutput, { name: 'userFeed' })
 @UseGuards(JwtAuthGuard)
 public async getUserFeed(
  @Args('args') args: FeedPaginationArgs,
  @Args('where') where: FeedWhereSelectInputDTO,
  @Context() ctx,
 ) {
  const { topic } = where
  // get the user id
  const { user } = ctx.req
  return this.feedService.getUserFeed(args, topic, user.id)
 }

 @Query(() => PaginatedFeedOutput, { name: 'guestUserFeed' })
 public async getGuestUserFeed(
  @Args('args') args: FeedPaginationArgs,
  @Args('where') where: FeedWhereSelectInputDTO,
 ) {
  const { topic } = where
  return this.feedService.getGuestUserFeed(args, topic)
 }
}
