import { ObjectType } from '@nestjs/graphql'
import { PaginatedResult } from '../../common/types/paginated-result'
import { Post } from '../../posts/posts.entity'

@ObjectType()
export class PaginatedFeedOutput extends PaginatedResult(Post) {}
