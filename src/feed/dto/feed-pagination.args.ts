import { Field, InputType } from '@nestjs/graphql'
import { IsNumber, IsOptional } from 'class-validator'

@InputType()
export class FeedPaginationArgs {
 @Field({
  nullable: true,
 })
 @IsNumber()
 @IsOptional()
 currentPage?: number = 0

 @Field({
  nullable: true,
 })
 @IsNumber()
 @IsOptional()
 pageSize?: number = 10
}
