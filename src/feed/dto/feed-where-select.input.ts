import { Field, InputType } from '@nestjs/graphql'
import { IsOptional, IsString } from 'class-validator'

@InputType()
export class FeedWhereSelectInputDTO {
 @Field(() => String, { nullable: true })
 @IsString()
 @IsOptional()
 topic?: string = ''
}
