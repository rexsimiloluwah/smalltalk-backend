import { Test, TestingModule } from '@nestjs/testing'
import { PrismaService } from '../prisma/prisma.service'
import { FeedService } from './feed.service'

describe('FeedService', () => {
 let prismaService
 let feedService: FeedService

 const prismaServiceMock = () => ({
  user: {
   findFirst: jest.fn(),
   findMany: jest.fn(),
  },
  post: {
   findFirst: jest.fn(),
   findMany: jest.fn(),
   count: jest.fn(),
  },
  $queryRaw: jest.fn(),
 })

 beforeEach(async () => {
  const module: TestingModule = await Test.createTestingModule({
   providers: [
    FeedService,
    {
     provide: PrismaService,
     useFactory: prismaServiceMock,
    },
   ],
  }).compile()

  feedService = module.get<FeedService>(FeedService)
  prismaService = module.get<PrismaService>(PrismaService)
 })

 it('should be defined', () => {
  expect(feedService).toBeDefined()
 })
})
