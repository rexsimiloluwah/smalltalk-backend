import { Test, TestingModule } from '@nestjs/testing'
import { FeedResolver } from './feed.resolver'
import { FeedService } from './feed.service'

describe('FeedResolver', () => {
 let resolver: FeedResolver
 let feedService

 const feedServiceMock = () => ({
  getUserFeed: jest.fn(),
  getGuestUserFeed: jest.fn(),
 })

 beforeEach(async () => {
  const module: TestingModule = await Test.createTestingModule({
   providers: [FeedResolver, { provide: FeedService, useFactory: feedServiceMock }],
  }).compile()

  resolver = module.get<FeedResolver>(FeedResolver)
  feedService = module.get<FeedService>(FeedService)
 })

 it('should be defined', () => {
  expect(resolver).toBeDefined()
 })
})
