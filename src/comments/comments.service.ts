import { ForbiddenException, Injectable } from '@nestjs/common'
import { PrismaService } from '../prisma/prisma.service'
import { Comment } from './comments.entity'
import { CommentWhereUniqueInputDTO } from './dto/comment-where-unique.input'
import { CreateCommentInputDTO } from './dto/create-comment.input'
import { UpdateCommentInputDTO } from './dto/update-comment.input'

export const COMMENT_WITH_USER_SELECT = {
 id: true,
 email: true,
 username: true,
 firstname: true,
 lastname: true,
 profileImageUrl: true,
}

export const COMMENT_INCLUDE = {
 creator: {
  select: COMMENT_WITH_USER_SELECT,
 },
}

@Injectable()
export class CommentsService {
 constructor(private prisma: PrismaService) {}

 // Create a comment
 async createComment(data: CreateCommentInputDTO, userId: number): Promise<Comment> {
  const { content, postId } = data
  const comment = await this.prisma.comment.create({
   data: {
    content: content,
    creator: {
     connect: {
      id: userId,
     },
    },
    post: {
     connect: {
      id: postId,
     },
    },
   },
  })

  return comment
 }

 // Update a comment
 async updateComment(
  data: UpdateCommentInputDTO,
  where: CommentWhereUniqueInputDTO,
  userId: number,
 ): Promise<Comment> {
  const comment = await this.findComment(where)
  if (comment.creatorId !== userId) {
   throw new ForbiddenException('You cannot update this comment.')
  }
  const updatedComment = await this.prisma.comment.update({
   where,
   data: data,
  })

  return updatedComment
 }

 // Delete a comment
 async deleteComment(where: CommentWhereUniqueInputDTO, userId: number): Promise<Comment> {
  const comment = await this.findComment(where)
  console.log("deleting comment", where)
  if (comment.creatorId !== userId) {
   throw new ForbiddenException('You cannot delete this comment.')
  }
  const deletedComment = await this.prisma.comment.delete({
   where: where,
  })

  return deletedComment
 }

 // Find a comment
 async findComment(where: CommentWhereUniqueInputDTO): Promise<Comment> {
  const comment = await this.prisma.comment.findFirst({
   where: where,
   orderBy: {
    createdAt: 'desc',
   },
   include: COMMENT_INCLUDE,
  })

  return comment
 }

 // Find post comments
 async findPostComments(postId: number): Promise<Comment[]> {
  const comments = await this.prisma.comment.findMany({
   where: {
    postId: postId,
   },
   orderBy: {
    createdAt: 'desc',
   },
   include: COMMENT_INCLUDE,
  })

  return comments
 }
}
