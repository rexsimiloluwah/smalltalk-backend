import { Test, TestingModule } from '@nestjs/testing'
import { CommentsResolver } from './comments.resolver'
import { CommentsService } from './comments.service'

describe('CommentsResolver', () => {
 let resolver: CommentsResolver
 let commentsService

 const mockCommentsService = () => ({
  createComment: jest.fn(),
  updateComment: jest.fn(),
  deleteComment: jest.fn(),
  findComment: jest.fn(),
  findPostComments: jest.fn(),
 })

 beforeEach(async () => {
  const module: TestingModule = await Test.createTestingModule({
   providers: [
    CommentsResolver,
    {
     provide: CommentsService,
     useFactory: mockCommentsService,
    },
   ],
  }).compile()

  resolver = module.get<CommentsResolver>(CommentsResolver)
  commentsService = module.get<CommentsService>(CommentsService)
 })

 it('should be defined', () => {
  expect(resolver).toBeDefined()
 })
})
