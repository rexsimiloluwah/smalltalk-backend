import { Test, TestingModule } from '@nestjs/testing'
import { CommentsService, COMMENT_WITH_USER_SELECT } from './comments.service'
import { PrismaService } from '../prisma/prisma.service'
import { Post } from '../posts/posts.entity'
import { User } from '../users/users.entity'
import { Comment } from './comments.entity'

const testPostOne: Post = {
 id: 1,
 title: 'Test post 1',
 description: 'Test description 1',
 creatorId: 1,
 audioFileUrl: 'https://s3.amazon.com/smalltalk/2022-12-02.mp3',
 createdAt: new Date().toISOString(),
 updatedAt: new Date().toISOString(),
 comments: [] as Comment[],
 creator: new User(),
}

const testCommentOne: Comment = {
 id: 1,
 content: 'Test comment 1',
 creatorId: 1,
 postId: 1,
 createdAt: new Date().toISOString(),
 updatedAt: new Date().toISOString(),
}

describe('CommentsService', () => {
 let commentsService: CommentsService
 let prismaService

 const prismaServiceMock = () => ({
  comment: {
   findUnique: jest.fn(),
   findFirst: jest.fn(),
   findMany: jest.fn(),
   count: jest.fn(),
   create: jest.fn(),
   upsert: jest.fn(),
   update: jest.fn(),
   updateMany: jest.fn(),
   delete: jest.fn(),
   deleteMany: jest.fn(),
  },
  $queryRaw: jest.fn(),
 })

 beforeEach(async () => {
  const module: TestingModule = await Test.createTestingModule({
   providers: [
    CommentsService,
    {
     provide: PrismaService,
     useFactory: prismaServiceMock,
    },
   ],
  }).compile()

  commentsService = module.get<CommentsService>(CommentsService)
  prismaService = module.get<PrismaService>(PrismaService)
 })

 it('should be defined', () => {
  expect(commentsService).toBeDefined()
 })

 // Test createComment service
 describe('createCommentService', () => {
  it('Should create a new comment', async () => {
   prismaService.comment.create.mockReturnValue(testCommentOne)
   const result = await commentsService.createComment({ content: 'Test Comment 1', postId: 1 }, 1)
   expect(result).toEqual(testCommentOne)
  })
 })

 // Test updateComment service
 describe('updateCommentService', () => {
  it('Should update a comment', async () => {
   const updatedComment = {
    ...testCommentOne,
    content: 'Updated comment',
   }
   prismaService.comment.findFirst.mockReturnValue(testCommentOne)
   prismaService.comment.update.mockReturnValue(updatedComment)
   const result = await commentsService.updateComment({ content: 'Updated comment' }, { id: 1 }, 1)
   expect(prismaService.comment.findFirst).toHaveBeenCalledWith({
    where: { id: 1 },
    include: {
     creator: {
      select: COMMENT_WITH_USER_SELECT,
     },
    },
    orderBy: {
     createdAt: 'desc',
    },
   })
   expect(result).toEqual(updatedComment)
  })
 })

 // Test deleteComment service
 describe('deleteCommentService', () => {
  it('Should delete a comment', async () => {
   prismaService.comment.findFirst.mockReturnValue(testCommentOne)
   prismaService.comment.delete.mockReturnValue(testCommentOne)
   const result = await commentsService.deleteComment({ id: 1 }, 1)
   expect(prismaService.comment.findFirst).toHaveBeenCalledWith({
    where: { id: 1 },
    include: {
     creator: {
      select: COMMENT_WITH_USER_SELECT,
     },
    },
    orderBy: {
     createdAt: 'desc',
    },
   })
   expect(result).toEqual(testCommentOne)
  })
 })

 // Test findComment service
 describe('findComment', () => {
  it('Should find a comment', async () => {
   prismaService.comment.findFirst.mockReturnValue(testCommentOne)
   const result = await commentsService.findComment({ id: 1 })
   expect(prismaService.comment.findFirst).toHaveBeenCalledWith({
    where: { id: 1 },
    include: {
     creator: {
      select: COMMENT_WITH_USER_SELECT,
     },
    },
    orderBy: {
     createdAt: 'desc',
    },
   })
   expect(result).toEqual(testCommentOne)
  })
 })

 // Test findPostComments service
 describe('findPostComments', () => {
  it("Should find a post's comments", async () => {
   prismaService.comment.findMany.mockReturnValue([testCommentOne])
   const result = await commentsService.findPostComments(1)
   expect(prismaService.comment.findMany).toHaveBeenCalledWith({
    where: { postId: 1 },
    include: {
     creator: {
      select: COMMENT_WITH_USER_SELECT,
     },
    },
    orderBy: {
     createdAt: 'desc',
    },
   })
   expect(result).toEqual([testCommentOne])
  })
 })
})
