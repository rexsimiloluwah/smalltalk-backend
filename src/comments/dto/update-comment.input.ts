import { Field, InputType } from '@nestjs/graphql'
import { IsNotEmpty, IsString } from 'class-validator'

@InputType()
export class UpdateCommentInputDTO {
 @Field(() => String, { nullable: false })
 @IsString()
 @IsNotEmpty()
 content: string
}
