import { Field, InputType } from '@nestjs/graphql'
import { IsNotEmpty, IsNumber, IsString } from 'class-validator'

@InputType()
export class CreateCommentInputDTO {
 @Field(() => String, { nullable: false })
 @IsString()
 @IsNotEmpty()
 content: string

 @Field(() => Number, { nullable: false })
 @IsNumber()
 postId: number
}
