import { Field, InputType } from '@nestjs/graphql'
import { IsNumber, IsOptional } from 'class-validator'

@InputType()
export class CommentWhereUniqueInputDTO {
 @Field(() => Number, {
  nullable: true,
 })
 @IsOptional()
 @IsNumber()
 id?: number
}
