import { UseGuards } from '@nestjs/common'
import { Args, Context, Mutation, Query, Resolver } from '@nestjs/graphql'
import { JwtAuthGuard } from '../auth/guards/jwt-auth.guard'
import { Comment } from './comments.entity'
import { CommentsService } from './comments.service'
import { CommentWhereUniqueInputDTO, CreateCommentInputDTO, UpdateCommentInputDTO } from './dto'

@Resolver(() => Comment)
export class CommentsResolver {
 constructor(private commentsService: CommentsService) {}

 @Mutation(() => Comment)
 @UseGuards(JwtAuthGuard)
 public async createComment(@Args('data') data: CreateCommentInputDTO, @Context() ctx) {
  // get user id
  const { user } = ctx.req
  const comment = await this.commentsService.createComment(data, user.id)
  return comment
 }

 @Query(() => [Comment], { name: 'comments' })
 public async findPostComments(@Args('where') where: CommentWhereUniqueInputDTO) {
  const comments = await this.commentsService.findPostComments(where.id)
  return comments
 }

 @Query(() => Comment, { name: 'comment' })
 public async findComment(@Args('where') where: CommentWhereUniqueInputDTO) {
  const comment = await this.commentsService.findComment(where)
  return comment
 }

 @Mutation(() => Comment)
 @UseGuards(JwtAuthGuard)
 public async updateComment(
  @Args('data') data: UpdateCommentInputDTO,
  @Args('where') where: CommentWhereUniqueInputDTO,
  @Context() ctx,
 ) {
  // get user id
  const { user } = ctx.req
  const comment = await this.commentsService.updateComment(data, where, user.id)
  return comment
 }

 @Mutation(() => Comment)
 @UseGuards(JwtAuthGuard)
 public async deleteComment(@Args('where') where: CommentWhereUniqueInputDTO, @Context() ctx) {
    console.log(where)
  // get user id
  const { user } = ctx.req
  const comment = await this.commentsService.deleteComment(where, user.id)
  return comment
 }
}
