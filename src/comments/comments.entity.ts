import { Field, ObjectType, ID } from '@nestjs/graphql'
import { User } from '../users/users.entity'
import { Post } from '../posts/posts.entity'

@ObjectType()
export class Comment {
 @Field(() => ID, { nullable: false })
 id: number

 @Field(() => String, { nullable: false })
 content: string

 @Field(() => ID, { nullable: false })
 postId: number

 @Field(() => ID, { nullable: false })
 creatorId: number

 @Field(() => Post, { nullable: true })
 post?: Post

 @Field(() => User, { nullable: true })
 creator?: User

 @Field(() => Date, { nullable: true })
 createdAt: Date | string

 @Field(() => Date, { nullable: true })
 updatedAt: Date | string
}
