import { NestFactory } from '@nestjs/core'
import { AppModule } from './app.module'
import { ValidationPipe } from '@nestjs/common'
import { NestExpressApplication } from '@nestjs/platform-express'
import * as session from 'express-session'
import * as passport from 'passport'
import { ConfigService } from '@nestjs/config'
import { json, urlencoded } from 'body-parser'

async function bootstrap() {
 const PORT = process.env.PORT || 3333
 const app = await NestFactory.create<NestExpressApplication>(AppModule)
 const configService = app.get(ConfigService)
 app.enableCors()
 app.use(json({ limit: '50mb' }))
 app.use(urlencoded({ limit: '50mb', extended: true }))
 app.use(
  session({
   secret: configService.get('SESSION_SECRET'),
   resave: false,
   saveUninitialized: true,
  }),
 )
 app.use(passport.initialize())
 app.use(passport.session())
 app.useGlobalPipes(
  new ValidationPipe({
   whitelist: true,
  }),
 )
 console.log('Application running on Heroku!...')
 await app.listen(PORT)
}
bootstrap()
