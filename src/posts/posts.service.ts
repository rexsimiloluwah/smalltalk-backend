import {
 ForbiddenException,
 HttpException,
 HttpStatus,
 Injectable,
 NotFoundException,
} from '@nestjs/common'
import { PrismaService } from '../prisma/prisma.service'
import {
 CreatePostInputDTO,
 ListPostsOutput,
 PostOrderByInput,
 PostPaginationArgs,
 PostWhereUniqueInputDTO,
 UpdatePostInputDTO,
} from './dto'
import { Like } from './like.entity'
import { Post } from './posts.entity'
import { Topic } from './topic.entity'

export const POSTS_WITH_USER_SELECT = {
 id: true,
 email: true,
 username: true,
 firstname: true,
 lastname: true,
 profileImageUrl: true,
}

export const POSTS_INCLUDE = {
 creator: {
  select: POSTS_WITH_USER_SELECT,
 },
 likes: {
  include: {
   user: {
    select: POSTS_WITH_USER_SELECT,
   },
  },
 },
 comments: {
  include: {
   creator: {
    select: POSTS_WITH_USER_SELECT,
   },
  },
 },
}

@Injectable()
export class PostsService {
 constructor(private prisma: PrismaService) {}

 // create a new topic
 async createTopic(name: string): Promise<Topic> {
  const newTopic = await this.prisma.topic.create({
   data: {
    name: name.toLowerCase(),
   },
  })
  return newTopic
 }

 // Create a new post
 async createPost(data: CreatePostInputDTO, userId: number): Promise<Post> {
  try {
   // Find the topic
   let topic = await this.findTopic(data.category)
   if (!topic) {
    // create a new topic
    topic = await this.createTopic(data.category)
   }

   const newPost = await this.prisma.post.create({
    data: {
     ...data,
     creator: {
      connect: {
       id: userId,
      },
     },
     topic: {
      connect: {
       id: topic.id,
      },
     },
    },
   })

   console.log(newPost)
   return newPost
  } catch (error) {
   if (error.status) {
    throw error
   }

   throw new HttpException(error.message, HttpStatus.INTERNAL_SERVER_ERROR)
  }
 }

 // Update a post
 async updatePost(
  where: PostWhereUniqueInputDTO,
  data: UpdatePostInputDTO,
  userId: number,
 ): Promise<Post> {
  try {
   const post = await this.findPostUnique({ id: where.id })
   if (!post) {
    throw new NotFoundException('Post not found.')
   }
   if (post.creatorId !== userId) {
    throw new ForbiddenException('A user can only update their post.')
   }
   return this.prisma.post.update({
    where: where,
    data: data,
   })
  } catch (error) {
   if (error.status) {
    throw error
   }

   throw new HttpException(error.message, HttpStatus.INTERNAL_SERVER_ERROR)
  }
 }

 // Delete a post
 async deletePost(where: PostWhereUniqueInputDTO, userId: number): Promise<Post> {
  try {
   const post = await this.findPostUnique({ id: where.id })
   if (!post) {
    throw new NotFoundException('Post not found.')
   }
   if (post.creatorId !== userId) {
    throw new ForbiddenException('A user can only delete their post.')
   }
   return this.prisma.post.delete({
    where: where,
   })
  } catch (error) {
   if (error.status) {
    throw error
   }

   throw new HttpException(error.message, HttpStatus.INTERNAL_SERVER_ERROR)
  }
 }

 // List all posts: to add pagination
 async listPosts(
  paginationArgs: PostPaginationArgs,
  orderBy: PostOrderByInput,
 ): Promise<ListPostsOutput> {
  const { take, endCursor, skip } = paginationArgs
  try {
   let posts: Post[] = []
   if (!endCursor) {
    posts = await this.prisma.post.findMany({
     take: take,
     skip: skip,
     orderBy: orderBy,
     include: POSTS_INCLUDE,
    })
   } else {
    posts = await this.prisma.post.findMany({
     take: take,
     skip: skip,
     cursor: {
      id: endCursor,
     },
     orderBy: orderBy,
     include: POSTS_INCLUDE,
    })
   }
   const totalCount = await this.prisma.post.count()
   const totalPages = Math.round(totalCount / take)

   // to calculate other pagination info
   if (posts.length > 0) {
    const lastPostInResults = posts[posts.length - 1]
    const endCursor = lastPostInResults.id

    // query after the cursor to see if we have a next page
    const nextPagePostsQuery = await this.prisma.post.findMany({
     take: take,
     cursor: {
      id: endCursor,
     },
    })
    //console.log(nextPagePostsQuery);

    // return the results
    return {
     posts: posts,
     pageInfo: {
      hasNextPage: nextPagePostsQuery.length >= take,
      totalCount: totalCount,
      totalPages: totalPages,
      endCursorId: endCursor,
     },
    }
   }

   return {
    posts,
    pageInfo: {
     hasNextPage: false,
     totalCount: 0,
     totalPages: 0,
     endCursorId: null,
    },
   }
  } catch (error) {
   if (error.status) {
    throw error
   }

   throw new HttpException(error.message, HttpStatus.INTERNAL_SERVER_ERROR)
  }
 }

 // Find post unique
 async findPostUnique(where: PostWhereUniqueInputDTO): Promise<Post> {
  try {
   const post = await this.prisma.post.findUnique({
    where: where,
    include: POSTS_INCLUDE,
   })
   if (post === null) {
    throw new NotFoundException('Post not found!')
   }
   return post
  } catch (error) {
   if (error.status) {
    throw error
   }

   throw new HttpException(error.message, HttpStatus.INTERNAL_SERVER_ERROR)
  }
 }

 // Find post(s) - not-unique
 async findPosts(where: PostWhereUniqueInputDTO): Promise<Post[]> {
  try {
   return this.prisma.post.findMany({
    where: where,
    include: POSTS_INCLUDE,
   })
  } catch (error) {
   if (error.status) {
    throw error
   }

   throw new HttpException(error.message, HttpStatus.INTERNAL_SERVER_ERROR)
  }
 }

 // Find posts by tag
 async findPostsByTag(tag: string): Promise<Post[]> {
  try {
   const posts = await this.prisma.post.findMany({
    where: {
     tags: {
      contains: tag,
     },
    },
    include: POSTS_INCLUDE,
   })

   return posts
  } catch (error) {
   if (error.status) {
    throw error
   }

   throw new HttpException(error.message, HttpStatus.INTERNAL_SERVER_ERROR)
  }
 }

 // Like a post
 async likePost(postId: number, userId: number): Promise<Like> {
  const like = await this.prisma.like.create({
   data: {
    user: {
     connect: {
      id: userId,
     },
    },
    post: {
     connect: {
      id: postId,
     },
    },
   },
   include: {
    user: {
     select: {
      id: true,
      email: true,
      username: true,
      firstname: true,
      lastname: true,
     },
    },
   },
  })

  return like
 }

 // Unlike a post
 async unlikePost(postId: number, userId: number): Promise<Like> {
  const like = await this.prisma.like.delete({
   where: {
    userId_postId: {
     userId: userId,
     postId: postId,
    },
   },
   include: {
    user: {
     select: {
      id: true,
      email: true,
      username: true,
      firstname: true,
      lastname: true,
     },
    },
   },
  })

  return like
 }

 // find a topic
 async findTopic(name: string): Promise<Topic | null> {
  const topic = await this.prisma.topic.findFirst({
   where: {
    name: name.toLowerCase(),
   },
   include: {
    posts: true,
   },
  })

  return topic
 }

 // Find popular topics
 async findTopics(): Promise<Topic[]> {
  const topics = await this.prisma.topic.findMany({
   orderBy: {
    posts: {
     _count: 'desc',
    },
   },
   include: {
    posts: true,
   },
  })

  return topics
 }
}
