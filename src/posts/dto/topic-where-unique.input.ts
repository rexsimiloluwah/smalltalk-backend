import { Field, ID, InputType } from '@nestjs/graphql'
import { IsNumber, IsOptional, IsString } from 'class-validator'

@InputType()
export class TopicWhereUniqueInputDTO {
 @Field(() => ID, { nullable: true })
 @IsNumber()
 @IsOptional()
 id?: number

 @Field(() => String, { nullable: true })
 @IsString()
 @IsOptional()
 name?: string
}
