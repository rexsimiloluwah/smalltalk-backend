import { Field, InputType } from '@nestjs/graphql'
import { IsNumber, IsOptional } from 'class-validator'

@InputType()
export class PostPaginationArgs {
 @Field({
  nullable: true,
 })
 @IsNumber()
 @IsOptional()
 endCursor?: number

 @Field({
  nullable: true,
 })
 @IsNumber()
 @IsOptional()
 take?: number = 10

 @Field({
  nullable: true,
 })
 @IsNumber()
 @IsOptional()
 skip?: number = 0
}
