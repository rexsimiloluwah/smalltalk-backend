import { Field, InputType } from '@nestjs/graphql'
import { IsOptional, IsString } from 'class-validator'

@InputType()
export class CreatePostArgs {
 @Field(() => String, { nullable: false })
 @IsString()
 title: string

 @Field(() => String, { nullable: true })
 @IsString()
 @IsOptional()
 description?: string

 @Field(() => String, { nullable: true })
 @IsString()
 @IsOptional()
 category?: string

 @Field(() => String, { nullable: true })
 @IsString()
 @IsOptional()
 tags?: string
}
