import { Field, InputType } from '@nestjs/graphql'
import { IsNotEmpty, IsOptional, IsString } from 'class-validator'

@InputType()
export class CreatePostInputDTO {
 @IsString()
 @IsNotEmpty()
 title: string

 @IsString()
 description?: string

 @Field(() => String, { nullable: true })
 @IsString()
 @IsOptional()
 category?: string

 @Field(() => String, { nullable: true })
 @IsString()
 @IsOptional()
 tags?: string

 @IsString()
 audioFileUrl: string
}
