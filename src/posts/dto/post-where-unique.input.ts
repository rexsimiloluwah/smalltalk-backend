import { Field, InputType } from '@nestjs/graphql'
import { IsNumber, IsOptional, IsString } from 'class-validator'

@InputType()
export class PostWhereUniqueInputDTO {
 @Field(() => Number, { nullable: true })
 @IsNumber()
 @IsOptional()
 id?: number

 @Field(() => Number, { nullable: true })
 @IsNumber()
 @IsOptional()
 creatorId?: number

 @Field(() => String, { nullable: true })
 @IsString()
 @IsOptional()
 category?: string

 @Field(() => String, { nullable: true })
 @IsString()
 @IsOptional()
 tag?: string
}
