import { Field, ObjectType } from '@nestjs/graphql'
import { Post } from '../posts.entity'

@ObjectType()
export class PageInfo {
 @Field(() => Boolean, { nullable: false })
 hasNextPage: boolean

 @Field(() => Number, { nullable: true })
 endCursorId: number

 @Field(() => Number, { nullable: false })
 totalPages: number

 @Field(() => Number, { nullable: false })
 totalCount: number
}

@ObjectType()
export class ListPostsOutput {
 @Field(() => [Post], { nullable: false })
 posts: Post[]

 @Field(() => PageInfo, { nullable: false })
 pageInfo: PageInfo
}
