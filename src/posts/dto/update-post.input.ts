import { Field, InputType } from '@nestjs/graphql'
import { IsOptional } from 'class-validator'

@InputType()
export class UpdatePostInputDTO {
 @Field(() => String, { nullable: true })
 @IsOptional()
 title?: string

 @Field(() => String, { nullable: true })
 @IsOptional()
 description?: string

 @Field(() => String, { nullable: true })
 @IsOptional()
 category?: string

 @Field(() => String, { nullable: true })
 @IsOptional()
 tags?: string
}
