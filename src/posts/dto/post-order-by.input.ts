import { Field, InputType } from '@nestjs/graphql'
import { IsOptional } from 'class-validator'
import { SortOrder } from '../../common/types/prisma/sort-order.enum'

@InputType()
export class PostOrderByInput {
 @Field(() => SortOrder, { nullable: true })
 @IsOptional()
 id?: SortOrder

 @Field(() => SortOrder, { nullable: true })
 @IsOptional()
 title?: SortOrder

 @Field(() => SortOrder, { nullable: true })
 @IsOptional()
 createdAt?: SortOrder

 @Field(() => SortOrder, { nullable: true })
 @IsOptional()
 updatedAt?: SortOrder
}
