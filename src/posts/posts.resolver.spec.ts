import { Test, TestingModule } from '@nestjs/testing'
import { PostsResolver } from './posts.resolver'
import { PostsService } from './posts.service'
import { Post } from './posts.entity'
import { Comment } from '../comments/comments.entity'
import { User } from '../users/users.entity'
import { CreatePostInputDTO } from './dto'
import { SortOrder } from '../common/types/prisma/sort-order.enum'

const testPostOne: Post = {
 id: 1,
 title: 'Test post 1',
 description: 'Test description 1',
 creatorId: 1,
 topicId: 1,
 audioFileUrl: 'https://s3.amazon.com/smalltalk/2022-12-02.mp3',
 createdAt: new Date().toISOString(),
 updatedAt: new Date().toISOString(),
 comments: [] as Comment[],
 creator: new User(),
}

const testPostTwo: Post = {
 id: 2,
 title: 'Test post 2',
 description: 'Test description 2',
 creatorId: 1,
 topicId: 1,
 audioFileUrl: 'https://s3.amazon.com/smalltalk/2022-12-02.mp3',
 createdAt: new Date().toISOString(),
 updatedAt: new Date().toISOString(),
 comments: [] as Comment[],
 creator: new User(),
}

const createPostInput: CreatePostInputDTO = {
 title: 'Test post 1',
 description: 'Test description 1',
 category: 'Test category',
 audioFileUrl: 'https://s3.amazon.com/smalltalk/2022-12-02.mp3',
}

const testPosts: Post[] = [testPostOne, testPostTwo]

describe('PostsResolver', () => {
 let resolver: PostsResolver
 let postsService

 const postsServiceMock = () => ({
  createPost: jest.fn(),
  updatePost: jest.fn(),
  deletePost: jest.fn(),
  listPosts: jest.fn(),
  findPosts: jest.fn(),
  findPostComments: jest.fn(),
  findPostUnique: jest.fn(),
  findPostsByTag: jest.fn(),
  likePost: jest.fn(),
  unlikePost: jest.fn(),
 })

 beforeEach(async () => {
  const module: TestingModule = await Test.createTestingModule({
   providers: [
    PostsResolver,
    {
     provide: PostsService,
     useFactory: postsServiceMock,
    },
   ],
  }).compile()

  resolver = module.get<PostsResolver>(PostsResolver)
  postsService = module.get<PostsService>(PostsService)
 })

 it('should be defined', () => {
  expect(resolver).toBeDefined()
 })

 // Test listPosts resolver
 describe('listPostsResolver', () => {
  it('Should list posts', async () => {
   const postsList = {
    posts: testPosts,
    pageInfo: {
     hasNextPage: false,
     totalCount: 2,
     totalPages: 0,
     endCursorId: 2,
    },
   }
   postsService.listPosts.mockReturnValue(postsList)
   const result = await resolver.listPosts({ take: 10, skip: 0 }, { id: SortOrder.asc })
   expect(result).toEqual(postsList)
  })
 })

 // Test findPost resolver
 describe('findPostResolver', () => {
  it('Should find post.', async () => {
   postsService.findPostUnique.mockReturnValue(testPostOne)
   const result = await resolver.findPost({ id: 1 })
   expect(result).toEqual(testPostOne)
  })
 })

 // Test findPosts resolver
 describe('findPostsResolver', () => {
  it('Should find posts', async () => {
   postsService.findPosts.mockReturnValue(testPosts)
   const result = await resolver.findPosts({})
   expect(result).toEqual(testPosts)
  })
 })

 // Test findPostsByCategory resolver
 describe('findPostsByCategoryResolver', () => {
  it('Should find posts by category', async () => {
   postsService.findPosts.mockReturnValue([testPostOne])
   const result = await resolver.findPostsByCategory('finance')
   expect(result).toEqual([testPostOne])
  })
 })

 // Test findPostsByTag resolver
 describe('findPostByTagResolver', () => {
  it('Should find posts by tag', async () => {
   postsService.findPostsByTag.mockReturnValue([testPostOne])
   const result = await resolver.findPostsByTag('finance')
   expect(result).toEqual([testPostOne])
  })
 })

 // Test createPost resolver

 // Test updatePost resolver

 // Test deletePost resolver

 // Test likePost resolver

 // Test unlikePost resolver
})
