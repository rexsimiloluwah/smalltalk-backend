import { Mutation, Resolver, Args, Query, Context } from '@nestjs/graphql'
import { GraphQLUpload, FileUpload } from 'graphql-upload'
import { Post } from './posts.entity'
import { PostsService } from './posts.service'
import {
 UpdatePostInputDTO,
 PostWhereUniqueInputDTO,
 PostPaginationArgs,
 ListPostsOutput,
 CreatePostArgs,
 PostOrderByInput,
 TopicWhereUniqueInputDTO,
} from './dto'
import { BadRequestException, UseGuards } from '@nestjs/common'
import { JwtAuthGuard } from '../auth/guards/jwt-auth.guard'
import { uploadFileS3 } from '../common/utils/upload.handler'
import { Like } from './like.entity'
import { Topic } from './topic.entity'

@Resolver(() => Post)
export class PostsResolver {
 constructor(private postService: PostsService) {}

 // List posts: paginated
 @Query(() => ListPostsOutput, { name: 'posts' })
 public async listPosts(
  @Args('args') args: PostPaginationArgs,
  @Args('orderBy') orderBy: PostOrderByInput,
 ): Promise<ListPostsOutput> {
  const posts = await this.postService.listPosts(args, orderBy)
  console.log(posts)
  return posts
 }

 // Find post
 @Query(() => Post, { name: 'post' })
 public async findPost(@Args('where') where: PostWhereUniqueInputDTO) {
  return this.postService.findPostUnique(where)
 }

 // Find posts
 @Query(() => [Post])
 public async findPosts(@Args('where') where: PostWhereUniqueInputDTO) {
  console.log(where)
  return this.postService.findPosts(where)
 }

 // Find posts by category
 @Query(() => [Post], { name: 'postsByCategory' })
 public async findPostsByCategory(@Args('category', { type: () => String }) category: string) {
  return this.postService.findPosts({ category: category })
 }

 // Find posts by tag
 @Query(() => [Post], { name: 'postsByTag' })
 public async findPostsByTag(@Args('tag', { type: () => String }) tag: string) {
  return this.postService.findPostsByTag(tag)
 }

 // Create a post
 @Mutation(() => Post)
 @UseGuards(JwtAuthGuard)
 public async createPost(
  @Context() ctx,
  @Args('data') data: CreatePostArgs,
  @Args('file', { type: () => GraphQLUpload }) file: FileUpload,
 ): Promise<Post> {
  const { user } = ctx.req
  // Local storage option
  // return new Promise(async (resolve, reject) => {
  //   const { createReadStream, filename } = file;
  //   console.log(file);
  //   createReadStream()
  //     .pipe(createWriteStream(`./uploads/${filename}`))
  //     .on("finish", () => resolve(true))
  //     .on("error", () => reject(false));
  // });
  try {
   const uploadResult = await uploadFileS3(file)
   console.log(uploadResult)
   return this.postService.createPost(
    {
     ...data,
     audioFileUrl: uploadResult.Location,
    },
    Number(user.id),
   )
  } catch (error) {
   console.log(error)
   throw new BadRequestException('An error occurred while creating post.')
  }
 }

 // Update a post
 @Mutation(() => Post)
 @UseGuards(JwtAuthGuard)
 public async updatePost(
  @Context() ctx,
  @Args('where') where: PostWhereUniqueInputDTO,
  @Args('data') data: UpdatePostInputDTO,
 ) {
  // get the user id
  const { user } = ctx.req
  const updatedPost = await this.postService.updatePost(where, data, user.id)
  return updatedPost
 }

 // Delete a post
 @Mutation(() => Post)
 @UseGuards(JwtAuthGuard)
 public async deletePost(@Args('where') where: PostWhereUniqueInputDTO, @Context() ctx) {
  // get the user id
  const { user } = ctx.req
  return this.postService.deletePost(where, user.id)
 }

 // Like a post
 @Mutation(() => Like)
 @UseGuards(JwtAuthGuard)
 public async likePost(@Args('where') where: PostWhereUniqueInputDTO, @Context() ctx) {
  // get the user id
  const { user } = ctx.req
  return this.postService.likePost(where.id, user.id)
 }

 // Unlike a post
 @Mutation(() => Like)
 @UseGuards(JwtAuthGuard)
 public async unlikePost(@Args('where') where: PostWhereUniqueInputDTO, @Context() ctx) {
  // get the user id
  const { user } = ctx.req
  return this.postService.unlikePost(where.id, user.id)
 }

 // Find topic
 @Query(() => Topic, { name: 'topic' })
 public async findTopic(@Args('where') where: TopicWhereUniqueInputDTO) {
  return this.postService.findTopic(where.name)
 }

 // Find popular topics
 @Query(() => [Topic], { name: 'topics' })
 public async findTopics() {
  return this.postService.findTopics()
 }
}
