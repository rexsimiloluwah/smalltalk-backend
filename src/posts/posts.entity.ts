import { Field, ObjectType, ID } from '@nestjs/graphql'
import { Comment } from '../comments/comments.entity'
import { User } from '../users/users.entity'
import { Like } from './like.entity'
import { Topic } from './topic.entity'

@ObjectType()
export class Post {
 @Field(() => ID, { nullable: false })
 id: number

 @Field(() => String, { nullable: false })
 title: string

 @Field(() => String, { nullable: false })
 audioFileUrl: string

 @Field(() => String, { nullable: true })
 description?: string

 @Field(() => ID, { nullable: false })
 creatorId: number

 @Field(() => User, { nullable: true })
 creator?: User

 @Field(() => ID, { nullable: true })
 topicId?: number

 @Field(() => Topic, { nullable: true })
 topic?: Topic

 @Field(() => String, { nullable: true })
 category?: string

 @Field(() => String, { nullable: true })
 tags?: string

 @Field(() => [Comment], { nullable: true })
 comments?: Comment[]

 @Field(() => [Like], { nullable: true })
 likes?: Like[]

 @Field(() => Date, { nullable: true })
 createdAt: Date | string

 @Field(() => Date, { nullable: true })
 updatedAt: Date | string
}
