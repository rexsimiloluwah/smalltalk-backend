import { ObjectType, Field, ID } from '@nestjs/graphql'
import { Post } from './posts.entity'

@ObjectType()
export class Topic {
 @Field(() => ID, { nullable: false })
 id: number

 @Field(() => String, { nullable: false })
 name: string

 @Field(() => [Post], { nullable: false })
 posts?: Post[]

 @Field(() => Date, { nullable: true })
 createdAt: Date | string

 @Field(() => Date, { nullable: true })
 updatedAt: Date | string
}
