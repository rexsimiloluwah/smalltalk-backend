import { Test, TestingModule } from '@nestjs/testing'
import { PostsService } from './posts.service'
import { PrismaService } from '../prisma/prisma.service'
import { Post } from './posts.entity'
import { Comment } from '../comments/comments.entity'
import { User } from '../users/users.entity'
import { CreatePostInputDTO } from './dto'
import { SortOrder } from '../common/types/prisma/sort-order.enum'
import { Topic } from './topic.entity'

const testPostOne: Post = {
 id: 1,
 title: 'Test post 1',
 description: 'Test description 1',
 creatorId: 1,
 topicId: 1,
 audioFileUrl: 'https://s3.amazon.com/smalltalk/2022-12-02.mp3',
 createdAt: new Date().toISOString(),
 updatedAt: new Date().toISOString(),
 comments: [] as Comment[],
 creator: new User(),
}

const testTopicOne: Topic = {
 id: 1,
 name: 'Test topic 1',
 posts: [] as Post[],
 createdAt: new Date().toISOString(),
 updatedAt: new Date().toISOString(),
}

const testPostTwo: Post = {
 id: 2,
 title: 'Test post 2',
 description: 'Test description 2',
 creatorId: 1,
 topicId: 1,
 audioFileUrl: 'https://s3.amazon.com/smalltalk/2022-12-02.mp3',
 createdAt: new Date().toISOString(),
 updatedAt: new Date().toISOString(),
 comments: [] as Comment[],
 creator: new User(),
}

const createPostInput: CreatePostInputDTO = {
 title: 'Test post 1',
 description: 'Test description 1',
 category: 'Test category',
 audioFileUrl: 'https://s3.amazon.com/smalltalk/2022-12-02.mp3',
}

const testPosts: Post[] = [testPostOne, testPostTwo]

describe('PostsService', () => {
 let postsService: PostsService
 let prismaService

 const prismaServiceMock = () => ({
  post: {
   findUnique: jest.fn(),
   findFirst: jest.fn(),
   findMany: jest.fn(),
   count: jest.fn(),
   create: jest.fn(),
   upsert: jest.fn(),
   update: jest.fn(),
   updateMany: jest.fn(),
   delete: jest.fn(),
   deleteMany: jest.fn(),
  },
  topic: {
   findFirst: jest.fn(),
   create: jest.fn(),
  },
  $queryRaw: jest.fn(),
 })

 beforeEach(async () => {
  const module: TestingModule = await Test.createTestingModule({
   providers: [
    PostsService,
    {
     provide: PrismaService,
     useFactory: prismaServiceMock,
    },
   ],
  }).compile()

  postsService = module.get<PostsService>(PostsService)
  prismaService = module.get<PrismaService>(PrismaService)
 })

 it('should be defined', () => {
  expect(postsService).toBeDefined()
 })

 // Test create post service
 describe('createPostService', () => {
  it('Should create post', async () => {
   prismaService.topic.create.mockReturnValue(testTopicOne)
   prismaService.post.create.mockReturnValue(testPostOne)
   const result = await postsService.createPost(createPostInput, 1)
   expect(result).toMatchObject(testPostOne)
  })
 })

 // Test list posts service
 describe('listPostsService', () => {
  it('Should list posts', async () => {
   prismaService.post.findMany.mockReturnValue(testPosts)
   prismaService.post.count.mockReturnValue(testPosts.length)
   const result = await postsService.listPosts({ take: 10, skip: 0 }, { id: SortOrder.asc })
   expect(result).toMatchObject({
    posts: testPosts,
    pageInfo: {
     hasNextPage: false,
     totalCount: 2,
     totalPages: 0,
     endCursorId: 2,
    },
   })
  })
 })

 // Test update post service
 describe('updatePostService', () => {
  it('Should update post', async () => {
   const updatedPost: Post = {
    ...testPostOne,
    title: 'Updated title',
   }
   prismaService.post.findUnique.mockReturnValue(testPostOne)
   prismaService.post.update.mockReturnValue(updatedPost)
   const result = await postsService.updatePost({ id: 1 }, { title: 'Updated title' }, 1)
   expect(result).toEqual(updatedPost)
  })
 })

 // Test delete post service
 describe('deletePostService', () => {
  it('Should delete post', async () => {
   prismaService.post.findUnique.mockReturnValue(testPostOne)
   prismaService.post.delete.mockReturnValue(testPostOne)
   const result = await postsService.deletePost({ id: 1 }, 1)
   expect(result).toEqual(testPostOne)
  })
 })

 // Test find post unique service
 describe('findPostUnique', () => {
  it('Should find unique post', async () => {
   prismaService.post.findUnique.mockReturnValue(testPostOne)
   const result = await postsService.findPostUnique({ id: 1 })
   expect(result).toEqual(testPostOne)
  })
 })

 // Test find posts service
 describe('findPosts', () => {
  it('Should find posts', async () => {
   prismaService.post.findMany.mockReturnValue(testPosts)
   const result = await postsService.findPosts({})
   expect(result).toEqual(testPosts)
  })
 })

 // Test like post service

 // Test unlike post service
})
