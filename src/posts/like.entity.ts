import { ObjectType, Field, ID } from '@nestjs/graphql'
import { User } from '../users/users.entity'
import { Post } from './posts.entity'

@ObjectType()
export class Like {
 @Field(() => ID, { nullable: false })
 id: number

 @Field(() => ID, { nullable: false })
 userId: number

 @Field(() => ID, { nullable: false })
 postId: number

 @Field(() => User, { nullable: false })
 user?: User

 @Field(() => Post, { nullable: false })
 post?: Post

 @Field(() => Date, { nullable: true })
 createdAt: Date | string

 @Field(() => Date, { nullable: true })
 updatedAt: Date | string
}
