import { Field, Int, ObjectType } from '@nestjs/graphql'
import { Type } from '@nestjs/common'

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function PaginatedResult<T>(ItemType: Type<T>): any {
 @ObjectType({ isAbstract: true })
 abstract class PageClass {
  @Field(() => [ItemType])
  data: T[]

  @Field(() => Int)
  totalPages: number

  @Field(() => Int)
  totalCount: number

  @Field(() => Boolean)
  hasNextPage: number

  @Field(() => Int)
  currentPage: number
 }

 return PageClass
}
