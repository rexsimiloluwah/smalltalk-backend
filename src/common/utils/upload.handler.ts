import * as AWS from 'aws-sdk'
import { FileUpload } from 'graphql-upload'

const S3 = new AWS.S3({
 accessKeyId: process.env.AWS_ACCESS_KEY_ID,
 secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
 region: process.env.AWS_REGION,
 apiVersion: '2006-03-01',
 signatureVersion: 'v4',
})

export const uploadFileS3 = async (file: FileUpload) => {
 const { createReadStream } = file
 const fileStream = createReadStream()
 // const fileExt = filename.split(".")[filename.split(".").length - 1];
 const s3FileKey = `${new Date().getTime().toString()}.mp3`

 const uploadParams = {
  Bucket: process.env.AWS_S3_BUCKET_NAME,
  Key: s3FileKey,
  Body: fileStream,
  ContentType: 'audio/mp3',
  ContentDisposition: 'attachment',
 }
 return S3.upload(uploadParams).promise()
}
