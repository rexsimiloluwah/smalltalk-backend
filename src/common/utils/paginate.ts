import { Post } from '../../posts/posts.entity'
import { User } from '../../users/users.entity'
import { Comment } from '../../comments/comments.entity'
import { PaginatedResult } from '../types/paginated-result'

export type PaginatedItem = Post | User | Comment

export type PaginatedResult = {
 data: PaginatedItem[]
 totalCount: number
 totalPages: number
 hasNextPage: boolean
 currentPage: number
}

export const paginate = (
 totalItems: PaginatedItem[],
 currentPage: number,
 pageSize: number,
): PaginatedResult => {
 const paginatedItems = totalItems.slice(currentPage * pageSize, (currentPage + 1) * pageSize)

 const response = {
  data: paginatedItems,
  totalCount: totalItems.length,
  totalPages: Math.floor(totalItems.length / pageSize),
  hasNextPage: totalItems.slice((currentPage + 1) * pageSize).length > 0,
  currentPage: currentPage,
 }

 return response
}

export const sortByCreatedAt = (items: PaginatedItem[]): PaginatedItem[] =>
 items.sort((a, b) => new Date(a.createdAt).getTime() - new Date(b.createdAt).getTime())

export const sortByNewest = (items: PaginatedItem[]): PaginatedItem[] =>
 sortByCreatedAt(items).reverse()
