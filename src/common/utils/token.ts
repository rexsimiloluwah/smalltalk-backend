import crypto from 'crypto'

export const generateRandomTokenString = () => {
 return crypto.randomBytes(40).toString('hex')
}
