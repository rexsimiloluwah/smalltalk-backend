import { Injectable, OnModuleInit, INestApplication } from '@nestjs/common'
import { PrismaClient } from '@prisma/client'

@Injectable()
export class PrismaService extends PrismaClient implements OnModuleInit {
 async onModuleInit() {
  await this.$connect()
 }

 async enableShutdownHooks(app: INestApplication) {
  this.$on('beforeExit', async () => {
   await app.close()
  })
 }

 async cleanDb() {
  return this.$transaction([
   this.user.deleteMany(),
   this.post.deleteMany(),
   this.comment.deleteMany(),
   this.profile.deleteMany(),
   this.like.deleteMany(),
   this.topic.deleteMany(),
  ])
 }
}
