import { Module } from '@nestjs/common'
import {MailerModule as NestMailerModule} from "@nestjs-modules/mailer"
import { ConfigModule, ConfigService } from '@nestjs/config'
import { join } from 'path'
import {HandlebarsAdapter} from "@nestjs-modules/mailer/dist/adapters/handlebars.adapter"

@Module({
    imports: [
        NestMailerModule.forRootAsync({
            imports: [ConfigModule],
            useFactory: async (config: ConfigService) => ({
                transport: {
                    host: config.get("EMAIL_HOST"),
                    secure: false,
                    auth: {
                        user: config.get("EMAIL_USER"),
                        pass: config.get("EMAIL_PASSWORD")
                    }
                },
                defaults: {
                    from: "rexsimiloluwa@gmail.com"
                },
                template: {
                    dir: join(__dirname, './templates'),
                    adapter: new HandlebarsAdapter(),
                    options: {
                        strict: true
                    }
                }
            }),
            inject: [ConfigService]
        }),
    ]
})
export class MailerModule {}
