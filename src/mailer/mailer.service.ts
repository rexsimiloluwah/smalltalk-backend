import { MailerService as NestMailerService } from "@nestjs-modules/mailer";
import { Injectable } from "@nestjs/common";

@Injectable()
export class MailerService{
    constructor(private mailerService: NestMailerService) {}

    async sendRegistrationMail(email: string, name: string) {
        return await this.mailerService.sendMail({
            to: email,
            subject: "Greeting from NestJS Nodemailer",
            template: "./email",
            context: {
                name: name
            }
        })
    }
}