import { Injectable } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'
import { PassportStrategy } from '@nestjs/passport'
import { ExtractJwt, Strategy } from 'passport-jwt'
import { PrismaService } from '../../prisma/prisma.service'

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
 constructor(private readonly config: ConfigService, private prisma: PrismaService) {
  super({
   jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
   ignoreExpiration: false,
   secretOrKey: config.get('JWT_SECRET_KEY'),
  })
 }

 // eslint-disable-next-line @typescript-eslint/no-explicit-any
 async validate(payload: any) {
  try {
   const user = await this.prisma.user.findUnique({
    where: {
     email: payload.email,
    },
    include: {
     profile: true,
     followers: {
      select: {
       user: true,
      },
     },
     following: {
      select: {
       user: true,
      },
     },
     posts: true,
    },
   })

   // remove password from the user object
   delete user.password
   return user
  } catch (error) {
   console.error(error)
   return null
  }
 }
}
