import { Injectable } from '@nestjs/common'
import { ExtractJwt, Strategy } from 'passport-jwt'
import { PassportStrategy } from '@nestjs/passport'
import { UsersService } from '../../users/users.service'
import { ConfigService } from '@nestjs/config'
import { PrismaService } from '../../prisma/prisma.service'
import * as argon from 'argon2'

@Injectable()
export class JwtRefreshTokenStrategy extends PassportStrategy(Strategy, 'jwt-refresh-token') {
 constructor(
  private readonly userService: UsersService,
  private config: ConfigService,
  private prisma: PrismaService,
 ) {
  super({
   jwtFromRequest: ExtractJwt.fromHeader('x-auth-refresh-token'),
   ignoreExpiration: false,
   secretOrKey: config.get('JWT_SECRET_KEY'),
   passReqToCallback: true,
  })
 }

 async validate(req, payload) {
  //   console.log(payload.email)
  const where = {
   email: payload.email,
  }
  const user = await this.prisma.user.findUnique({
   where: where,
  })
  if (!user) return null

  const refreshToken = req?.headers['x-auth-refresh-token']
  const isRefreshTokenMatching = await argon.verify(user.hashedRefreshToken, refreshToken)

  // If refreshToken not match with token encrypted in database --> return null
  if (!isRefreshTokenMatching) return null

  return user
 }
}
