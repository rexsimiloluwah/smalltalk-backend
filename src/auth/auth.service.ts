import {
 BadRequestException,
 Injectable,
 NotFoundException,
 UnauthorizedException,
} from '@nestjs/common'
import { PrismaService } from '../prisma/prisma.service'
import { ConfigService } from '@nestjs/config'
import { LoginUserInputDTO, AccessTokenOutputDTO } from './dto'
import { User } from '../users/users.entity'
import * as argon from 'argon2'
import { CreateUserInputDTO } from '../users/dto'
import { JwtService } from '@nestjs/jwt'
import { UsersService } from '../users/users.service'
import { Auth, google } from 'googleapis'
import { GoogleLoginUserInputDTO } from './dto/google-login-user.input'
import { LoginUserOutputDTO } from './dto/login-user.output'

@Injectable()
export class AuthService {
 private oauthClient: Auth.OAuth2Client

 constructor(
  private prisma: PrismaService,
  private config: ConfigService,
  private jwtService: JwtService,
  private userService: UsersService,
 ) {
  this.oauthClient = new google.auth.OAuth2(
   this.config.get('GOOGLE_CLIENT_ID'),
   this.config.get('GOOGLE_CLIENT_SECRET'),
  )
 }

 // REGISTER: Create a new user
 async register(data: CreateUserInputDTO): Promise<Partial<User>> {
  return this.userService.createUser(data)
 }

 // LOGIN: Return access token
 async login(data: LoginUserInputDTO): Promise<LoginUserOutputDTO> {
  const { email, password } = data

  // find user by email
  const user = await this.prisma.user.findFirst({
   where: {
    email: email,
   },
  })

  // if the user does not exist, throw and exception
  if (!user) {
   throw new NotFoundException('User does not exist.')
  }

  // if the user exists, verify that the password is correct
  const passwordMatch = await argon.verify(user.password, password)

  // if the passwords do not match
  if (!passwordMatch) {
   throw new BadRequestException('Incorrect password.')
  }

  const accessToken = await this.generateNewJwtAccessToken(user.id, user.email)
  const refreshToken = await this.generateNewJwtRefreshToken(user.id, user.email)
  return {
   user: user,
   token: {
    ...accessToken,
    ...refreshToken,
   },
  }
 }

 // Refresh access token using the refreshToken
 async refreshToken(user: User): Promise<AccessTokenOutputDTO> {
  const { id, email } = user

  // Generate the access token
  const accessToken = await this.generateNewJwtAccessToken(id, email)

  return accessToken
 }

 // Login with google
 async loginWithGoogle(data: GoogleLoginUserInputDTO): Promise<LoginUserOutputDTO | undefined> {
  const { token } = data
  // decrypt the token
  const tokenInfo = await this.oauthClient.verifyIdToken({
   idToken: token,
   audience: this.config.get('GOOGLE_CLIENT_ID'),
  })
  const { name, picture, email, email_verified } = tokenInfo.getPayload()

  if (!email_verified) {
   throw new UnauthorizedException('Google email is not verified.')
  }
  // check if the user exists
  const user = await this.userService.findUser({ email: email })
  if (user) {
   const accessToken = await this.generateNewJwtAccessToken(user.id, user.email)
   const refreshToken = await this.generateNewJwtRefreshToken(user.id, user.email)
   return {
    user: user,
    token: {
     ...accessToken,
     ...refreshToken,
    },
   }
  }
  // if the user does not exist, create a new user
  const newUser = await this.userService.createUser({
   email: email,
   firstname: name.split(' ')[0],
   lastname: name.split(' ')[1],
   password: email + this.config.get('JWT_SECRET'),
   emailVerified: true,
   profileImageUrl: picture,
   username: name.split(' ').join(''),
  })

  const accessToken = await this.generateNewJwtAccessToken(newUser.id, newUser.email)
  const refreshToken = await this.generateNewJwtRefreshToken(newUser.id, newUser.email)

  delete newUser['password']
  return {
   user: newUser,
   token: {
    ...accessToken,
    ...refreshToken,
   },
  }
 }

 // Generate a new access token
 async generateNewJwtAccessToken(userId: number, email: string): Promise<{ accessToken: string }> {
  const payload = {
   sub: userId,
   email,
  }

  const secretKey = this.config.get('JWT_SECRET_KEY')
  const accessTokenExpiresIn = this.config.get('JWT_EXPIRES_IN')
  // Generate the access token
  const accessToken = await this.jwtService.signAsync(payload, {
   expiresIn: accessTokenExpiresIn,
   secret: secretKey,
  })

  return {
   accessToken: accessToken,
  }
 }

 // Generate a new refresh token
 async generateNewJwtRefreshToken(
  userId: number,
  email: string,
 ): Promise<{ refreshToken: string }> {
  const payload = {
   sub: userId,
   email,
  }
  const secretKey = this.config.get('JWT_SECRET_KEY')
  const refreshTokenExpiresIn = this.config.get('REFRESH_TOKEN_EXPIRES_IN')

  // Generate the refresh token
  const refreshToken = await this.jwtService.signAsync(payload, {
   expiresIn: refreshTokenExpiresIn,
   secret: secretKey,
  })

  // Generate a hashed refresh token
  const hashedRefreshToken = await argon.hash(refreshToken)
  // console.log(hashedRefreshToken)
  // Update the user's hashed refresh token
  await this.userService.updateUser({ id: userId }, { hashedRefreshToken: hashedRefreshToken })

  // return the generated refresh token
  return {
   refreshToken: refreshToken,
  }
 }
}
