/* eslint-disable @typescript-eslint/ban-ts-comment */
import { ExecutionContext } from '@nestjs/common'
import { GqlExecutionContext } from '@nestjs/graphql'
import { Test, TestingModule } from '@nestjs/testing'
import { User } from '../../users/users.entity'
import { JwtAuthGuard } from './jwt-auth.guard'

const ctx = {
 getClass: jest.fn(),
 getHandler: jest.fn(),
 getContext: jest.fn(),
 getType: jest.fn(),
 getArgs: jest.fn(),
 getRoot: jest.fn(),
 getInfo: jest.fn(),
}

const mockGqlExecutionContext = () => ({
 create: jest.fn(),
})

describe('JwtAuthGuard', () => {
 let gqlGuard: JwtAuthGuard
 let gqlExecutionContext

 beforeAll(async () => {
  const module: TestingModule = await Test.createTestingModule({
   providers: [
    JwtAuthGuard,
    {
     provide: GqlExecutionContext,
     useFactory: mockGqlExecutionContext,
    },
   ],
  }).compile()

  gqlGuard = module.get<JwtAuthGuard>(JwtAuthGuard)
  gqlExecutionContext = module.get<GqlExecutionContext>(GqlExecutionContext)
 })

 // Test if it initializes with the PostService class
 it('Should be defined when init directly', () => {
  // @ts-ignore
  const { PostsService } = jest.createMockFromModule('../../posts/posts.service.ts')
  expect(new JwtAuthGuard(new PostsService())).toBeDefined()
 })

 it('should be defined', () => {
  expect(gqlGuard).toBeDefined()
 })
})
