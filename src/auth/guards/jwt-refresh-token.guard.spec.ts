/* eslint-disable @typescript-eslint/ban-ts-comment */
import { JwtRefreshTokenGuard } from './jwt-refresh-token.guard'

describe('JwtRefreshTokenGuard', () => {
 it('Should be defined', () => {
  // @ts-ignore
  const { PostsService } = jest.createMockFromModule('../../posts/posts.service.ts')
  expect(new JwtRefreshTokenGuard(new PostsService())).toBeDefined()
 })
})
