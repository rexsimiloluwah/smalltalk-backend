import { Test, TestingModule } from '@nestjs/testing'
import { AuthResolver } from './auth.resolver'
import { AuthService } from './auth.service'

describe('AuthResolver', () => {
 let resolver: AuthResolver

 const mockAuthService = () => ({
  register: jest.fn(),
  login: jest.fn(),
  refreshToken: jest.fn(),
  generateNewJwtRefreshToken: jest.fn(),
  generateNewJwtAccessToken: jest.fn(),
 })

 beforeEach(async () => {
  const module: TestingModule = await Test.createTestingModule({
   providers: [
    AuthResolver,
    {
     provide: AuthService,
     useFactory: mockAuthService,
    },
   ],
  }).compile()

  resolver = module.get<AuthResolver>(AuthResolver)
 })

 it('should be defined', () => {
  expect(resolver).toBeDefined()
 })
})
