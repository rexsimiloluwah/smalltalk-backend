import { Module } from '@nestjs/common'
import { AuthService } from './auth.service'
import { AuthResolver } from './auth.resolver'
import { JwtModule } from '@nestjs/jwt'
import { JwtRefreshTokenStrategy, JwtStrategy } from './jwt'
import { UsersService } from '../users/users.service'
import { MailerModule } from '../mailer/mailer.module'
import { MailerService } from '../mailer/mailer.service'

@Module({
 imports: [JwtModule.register({}), MailerModule],
 providers: [AuthService, UsersService, MailerService, AuthResolver, JwtStrategy, JwtRefreshTokenStrategy],
})
export class AuthModule {}
