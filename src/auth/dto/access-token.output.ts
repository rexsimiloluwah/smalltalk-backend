import { IsString } from 'class-validator'
import { Field, ObjectType } from '@nestjs/graphql'

@ObjectType()
export class AccessTokenOutputDTO {
 @Field(() => String)
 @IsString()
 accessToken: string

 @Field(() => String, { nullable: true })
 @IsString()
 refreshToken?: string
}
