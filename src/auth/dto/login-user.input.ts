import { IsEmail, IsNotEmpty, IsString } from 'class-validator'
import { Field, InputType } from '@nestjs/graphql'

@InputType()
export class LoginUserInputDTO {
 @Field(() => String, { nullable: false })
 @IsEmail()
 @IsString()
 email!: string

 @Field(() => String, { nullable: false })
 @IsString()
 @IsNotEmpty()
 password!: string
}
