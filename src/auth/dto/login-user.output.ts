import { Field, ObjectType } from '@nestjs/graphql'
import { User } from '../../users/users.entity'
import { AccessTokenOutputDTO } from './access-token.output'

@ObjectType()
export class LoginUserOutputDTO {
 @Field(() => User, { nullable: false })
 user: User

 @Field(() => AccessTokenOutputDTO, { nullable: false })
 token: AccessTokenOutputDTO
}
