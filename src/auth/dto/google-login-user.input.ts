import { IsString } from 'class-validator'
import { InputType, Field } from '@nestjs/graphql'

@InputType()
export class GoogleLoginUserInputDTO {
 @Field(() => String, { nullable: false })
 @IsString()
 token: string
}
