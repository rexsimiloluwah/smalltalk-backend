import { IsString } from 'class-validator'
import { InputType, Field } from '@nestjs/graphql'

@InputType()
export class RefreshTokenInputDTO {
 @Field(() => String, { nullable: false })
 @IsString()
 refreshToken: string
}
