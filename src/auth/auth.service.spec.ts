import { Test, TestingModule } from '@nestjs/testing'
import { AuthService } from './auth.service'
import { PrismaService } from '../prisma/prisma.service'
import { JwtModule } from '@nestjs/jwt'
import { ConfigService } from '@nestjs/config'
import { UsersService } from '../users/users.service'

describe('AuthService', () => {
 let authService: AuthService
 let prismaService

 const mockPrismaService = () => ({
  user: {
   findUnique: jest.fn(),
   findFirst: jest.fn(),
   findMany: jest.fn(),
   count: jest.fn(),
   create: jest.fn(),
   upsert: jest.fn(),
   update: jest.fn(),
   updateMany: jest.fn(),
   delete: jest.fn(),
   deleteMany: jest.fn(),
  },
  $queryRaw: jest.fn(),
 })

 const mockUsersService = () => ({
  createUser: jest.fn(),
  findUser: jest.fn(),
 })

 beforeEach(async () => {
  const module: TestingModule = await Test.createTestingModule({
   imports: [JwtModule.register({})],
   providers: [
    AuthService,
    { provide: PrismaService, useFactory: mockPrismaService },
    { provide: UsersService, useFactory: mockUsersService },
    {
     provide: ConfigService,
     useValue: {
      get: jest.fn((key: string) => {
       if (key === 'JWT_SECRET_KEY' || key === 'REFRESH_TOKEN_EXPIRES_IN') {
        return 'secret'
       }
       if (key === 'JWT_EXPIRES_IN') {
        return '24h'
       }
       return null
      }),
     },
    },
   ],
  }).compile()

  authService = module.get<AuthService>(AuthService)
  prismaService = module.get<PrismaService>(PrismaService)
 })

 it('should be defined', () => {
  expect(authService).toBeDefined()
 })
})
