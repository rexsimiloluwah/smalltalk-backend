import { Mutation, Resolver, Args, Context } from '@nestjs/graphql'
import { AuthService } from './auth.service'
import { User } from '../users/users.entity'
import {
 LoginUserInputDTO,
 AccessTokenOutputDTO,
 LoginUserOutputDTO,
 GoogleLoginUserInputDTO,
} from './dto'
import { CreateUserInputDTO } from '../users/dto'
import { UseGuards } from '@nestjs/common'
import { JwtRefreshTokenGuard } from './guards/jwt-refresh-token.guard'

@Resolver(() => User)
export class AuthResolver {
 constructor(private authService: AuthService) {}

 // Register Mutation
 @Mutation(() => User)
 public async register(@Args('data') data: CreateUserInputDTO) {
  const user = this.authService.register(data)
  return user
 }

 // Login Mutation
 @Mutation(() => LoginUserOutputDTO)
 public async login(@Args('data') data: LoginUserInputDTO) {
  const response = await this.authService.login(data)
  return response
 }

 // Login with google mutation
 @Mutation(() => LoginUserOutputDTO)
 public async loginWithGoogle(@Args('data') data: GoogleLoginUserInputDTO, @Context() ctx) {
  const response = await this.authService.loginWithGoogle(data)
  ctx.req.session.refreshToken = response.token.refreshToken
  return response
 }

 // Refresh Token Mutation
 @Mutation(() => AccessTokenOutputDTO)
 @UseGuards(JwtRefreshTokenGuard)
 public async refreshToken(@Context() ctx) {
  const req = ctx.req
  console.log(req.session.token)
  const { user } = req
  const newAccessToken = await this.authService.refreshToken(user)
  // Update session here also
  return newAccessToken
 }
}
