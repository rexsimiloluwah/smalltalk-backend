import { Module, NestModule, MiddlewareConsumer } from '@nestjs/common'
import { AppController } from './app.controller'
import { AppService } from './app.service'
import { UsersModule } from './users/users.module'
import { GraphQLModule } from '@nestjs/graphql'
import { ApolloDriver, ApolloDriverConfig } from '@nestjs/apollo'
import { join } from 'path'
import { PrismaModule } from './prisma/prisma.module'
import { ConfigModule } from '@nestjs/config'
import { AuthModule } from './auth/auth.module'
import { PostsModule } from './posts/posts.module'
import { graphqlUploadExpress } from 'graphql-upload'
import { CommentsModule } from './comments/comments.module'
import { MailerModule } from './mailer/mailer.module'
import { FeedModule } from './feed/feed.module'

@Module({
 imports: [
  ConfigModule.forRoot({
   isGlobal: true,
  }),
  GraphQLModule.forRoot<ApolloDriverConfig>({
   driver: ApolloDriver,
   autoSchemaFile: join(process.cwd(), 'src/schema.gql'),
   playground: {
    settings: {
     'request.credentials': 'include',
    },
   },
   introspection: true,
  }),
  UsersModule,
  PrismaModule,
  AuthModule,
  PostsModule,
  CommentsModule,
  MailerModule,
  FeedModule,
 ],
 controllers: [AppController],
 providers: [AppService],
})
export class AppModule implements NestModule {
 configure(consumer: MiddlewareConsumer) {
  consumer.apply(graphqlUploadExpress({ maxFileSize: 10000000, maxFiles: 10 })).forRoutes('/')
 }
}
